# Création d'un frameWork en PHP ( non fini )

Avec l'implémentation d'un petit projet de blog sur ce framework pour tester son bon fonctionnement

Fait en from Scratch à partir de mes propres connaissances  


### Installation : 

 * #### Assurez-vous d'avoir l'extension yaml installé et activer dans Php ini
 * #### Cloner le projet 
 * #### Créer une base de données nommée framework
 * #### Importer le fichier db-sql.sql dans votre base de données
 * #### Lancer la ligne de commande : *composer install*
 * #### Lancer la ligne de commande : *php -S localhost:8000 -t public*
 * #### Enjoy !



### Liste des fichiers :  

 * #### public, est le starter de l'application  
 * ####  settings, sont les configurations de notre framework
 * #### src, est notre application  
        * Controller, sont des routeurs    
        * Entity, sont nos modèles  
        * FormType, les formulaires  
        * FormeworkComponent, les composants d'écosystème de notre framework  
        * Repository, sont l'accès, gestions de nos données  
        * View, sont nos templates  
        
 * ####  Makeall.bat , fichier de directive  
 * #### script.php , fichier pour gérer les lignes de commandes  



