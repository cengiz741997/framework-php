-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 22 sep. 2021 à 13:39
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `framework`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(2500) NOT NULL,
  `likes` int(11) DEFAULT NULL,
  `tags` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `articles_ibfk_1` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `likes`, `tags`, `created_at`, `user_id`) VALUES
(1, 'Php le meilleur langage ', 'Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.', NULL, NULL, '2021-02-03 14:19:17', 1),
(2, 'Python le langage du hacking !', 'Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.', NULL, NULL, '2021-02-05 14:21:15', 1),
(3, 'Pourquoi développer ?', 'Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.', NULL, NULL, '2021-02-03 14:21:41', 1),
(4, 'Javascript et manié le DOM', 'Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.', NULL, NULL, '2021-02-10 14:22:14', 1),
(5, 'Jquery est t-il fini', 'Quisque sodales ipsum et augue auctor rhoncus. Praesent facilisis urna vitae finibus accumsan. Donec pretium mauris eu ligula lacinia aliquam. Cras imperdiet tempor nibh ac ullamcorper. Duis porta turpis neque, tincidunt pellentesque dui lobortis in. Mauris sed est porta, faucibus leo vitae, eleifend nisl. Proin ac velit in massa tincidunt accumsan. Ut a aliquam lacus. Praesent eget tempor odio. Donec justo felis, iaculis sed auctor sagittis, dapibus sed est. Vivamus congue mi ac quam pulvinar, sit amet euismod est mattis. Duis molestie turpis ac quam finibus, ut egestas ligula varius.', NULL, NULL, '2021-02-10 14:23:58', 1);

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `article_id` (`article_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `roles` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `firstname`, `email`, `password`, `city`, `cp`, `created_at`, `roles`) VALUES
(1, 'tom', 'thomas', 'khje@gmail.com', '', 'paris', '73000', '2021-02-23 14:18:06', ''),
(4, 'code', 'mika', 'mika@mail.com', '', 'paris', '73000', '2021-05-09 11:22:21', ''),
(21, 'kurt', 'cengiz', 'kurt@gmail.com', '', 'chens', '74140', '2021-05-09 12:01:58', ''),
(24, 'kurt', 'cengiz', 'kurtt.cengiz@gmail.com', '', 'chens sur leman', '74140', '2021-05-15 22:07:16', ''),
(29, 'kurt', 'cengiz', 'kdddurtt.cengiz@gmail.com', '', 'chens sur leman', '74140', '2021-07-17 12:33:57', ''),
(31, 'gurgune', 'olgay', 'cengiz@gmail.com', '', 'douvaine', '74140', '2021-07-17 12:37:02', '[\"ROLE_USER\",\"ROLE_ADMIN\"]'),
(32, 'thomas', 'jhony', 'ckue@gmail.com', '', 'france', '74140', '2021-09-22 15:38:31', '[\"ROLE_USER\",\"ROLE_ADMIN\"]');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Contraintes pour la table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`article_id`) REFERENCES `articles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
