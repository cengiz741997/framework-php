"use strict";
const y_max_nav_bar_event = 50;
const navbar = document.getElementsByClassName('navbar')[0];
const img_php_navbar = document.getElementsByClassName(' logo-php')[0];
document.addEventListener('scroll' , () => {
   let y = window.scrollY;
   if(y >= y_max_nav_bar_event) {
        navbar.classList.add('nav-reduct');
        img_php_navbar.classList.add('nav-reduct-logo');
   }else{
       navbar.classList.remove('nav-reduct');
       img_php_navbar.classList.remove('nav-reduct-logo');
   }
});