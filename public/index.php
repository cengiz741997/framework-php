<?php
session_start();

//var_dump($_SESSION);
//session_destroy();
//die;
require_once join(DIRECTORY_SEPARATOR, [__DIR__ . "\..\\", 'vendor', 'autoload.php']);

use FrameWork\FrameWorkComponent\Finder\Finder;
use FrameWork\FrameWorkComponent\Router;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\DotEnv;

date_default_timezone_set('Europe/Paris');

(new DotEnv(__DIR__ . '\..\.env'))->load();


if($_ENV['APP_ENV'] !== "prod") {
    error_reporting(E_ALL);
}else{
    error_reporting(0);
}

function exceptions_error_notice_handler($severity = null, $message = null, $filename = null, $line = null, $trace = null)
{
    $exception = new ExceptionApp($message, $severity, $filename, $line, $trace);
    return $exception->callError();
}

function exceptions_error_fatal_handler($contentErrorObjet)
{
    // crée la class exception fatal $contentErrorObjet est un objet
    $exception = new Fatal($contentErrorObjet);
    return $exception->callError();
}

//
//set_error_handler('exceptions_error_notice_handler');
//set_exception_handler('exceptions_error_fatal_handler');

$router = new Router();
$router->invokeController();
