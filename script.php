#!/usr/bin/php
<?php

/**
 * Class script
 */
class Script
{

    /**
     * @var array
     */
    private $allScripts = [];
    /**
     * @var string
     */
    private $argv = '';
    /**
     * @var string
     */
    private $pathScript = '/src/FrameWorkComponent/ScriptManagement/';
    /**
     * @var string
     */
    private $entityCall = '';
    /**
     * @var string
     */
    private $parentFile = '';
    /**
     * @var string
     */
    private $msgAllScript = '';

    /**
     * script constructor.
     */
    public function __construct()
    {
        $this->parentFile = dirname(__FILE__);
    }

    /**
     * @param string|null $scriptArg
     */
    public function commandConsole($scriptArg)
    {
        if (!$scriptArg) {
            echo "\033[31m Aucun argument passé [ ex : php script.php -entity ] \033[0m" . PHP_EOL;
            return;
        }
        $scriptName = str_replace('-', '', $scriptArg);
        $scriptName = ucfirst($scriptName);
        $this->entityCall = $scriptName;

        $this->allScript();
        $this->msgAllScript();

        if (strpos($scriptArg, 'help')) {
            echo $this->msgAllScript;
            return;
        }

        $pathToScript = $this->parentFile . $this->pathScript . $scriptName . '/' . $scriptName . '.php';

        $fileExist = file_exists($pathToScript);

        if ($fileExist) {
            return include $pathToScript;
        }

        $notFoundScript = "\033[31m Le script n'existe pas \033[0m" . PHP_EOL;
        $finalMessage = $notFoundScript . $this->msgAllScript;
        echo $finalMessage;

    }

    private function allScript()
    {
        // récup tous les scripts disponibles
        $allScripts = scandir($this->parentFile . $this->pathScript);
        $deleteFile = ['.', '..'];
        $this->allScripts = array_diff($allScripts, array('.', '..'));
    }

    private function msgAllScript()
    {
        $this->msgAllScript = "\033[32m Voci les différents scripts \033[0m" . PHP_EOL;
        foreach ($this->allScripts as $script) {
            $this->msgAllScript .= "- $script";
        }
    }
}

$scriptCommand = new Script();

$argvEntity = !empty($argv[1]) ? $argv[1] : null;
$scriptCommand->commandConsole($argvEntity);
