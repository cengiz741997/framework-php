<?php

namespace FrameWork\Controller;

use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\Controller\BaseController;
use FrameWork\FrameWorkComponent\Controller\ControllerInterface;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\ManagerController;
use FrameWork\FrameWorkComponent\RenderFile;
use FrameWork\Repository\ArticleRepository;
use FrameWork\FrameWorkComponent\Annotation\Worker;
use FrameWork\FrameWorkComponent\Annotation\WorkerInterface;
use FrameWork\FrameWorkComponent\Annotation\WorkerMethodController;

/**
 * Class ArticleController
 * @package FrameWork\Controller
 */
class ArticleController extends BaseController
{
    /**
     * @var array
     */
    private $url;

    /**
     * @var array
     */
    public static $link = [];

    /**
     * @var array
     */
    public static $assetsFile = [
        'js' => [],
        'css' => []
    ];

    /**
     * @var RenderFile
     */
    protected $response;

    /**
     * @var string
     */
    public static $titlePage = 'Article n° ';

    /**
     * ArticleController constructor.
     * @param $url
     */
    public function __construct($url)
    {
        parent::__construct();
        $this->response = $this->getComponent('RenderFile');
        $httpResponse = $this->getComponent('httpResponse');
        $this->url = $url;

    }

    /**
     * @return RenderFile
     * @throws \FrameWork\FrameWorkComponent\Error\ExceptionApp
     * @WorkerMethodController (
     *     name = "viewOneArticle",
     *     url = "/article/viewOne/{id}"
     * )
     */
    public function viewOneArticle(): RenderFile
    {

        $httpResponse = $this->getComponent('httpResponse');

        $articles = new ArticleRepository;
        $id = !empty($this->url['id']) ? $this->url['id'] : false;
        $article = $articles->findOneBy('id', $id);

        if (!$article) {
            return $httpResponse->call404();
        }
        $user = $this->getUser();
        $this::$titlePage .= strval($id);
        $this->response->response([getenv('PATH_TEMPLATE') . 'article.php'], [
            'article' => $article,
            'user' => $user
        ], $this);
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createUrl(ManagerController $managementController, string $controller)
    {
        $managementController->createUrl($controller, 'viewArticle', 'article/viewOne/[id]', ['id']);
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createAssets(ManagerController $managementController, string $controller)
    {
        $managementController->createAssets($controller, 'article.css', 'css');
    }

    /**
     * @param string $routeName
     * @return mixed|string
     * @throws Fatal
     */
    public function getRoute(string $routeName)
    {

    }
}