<?php

namespace FrameWork\Controller;

use FrameWork\FormType\ConnexionFormType;
use FrameWork\FormType\UserFormType;
use FrameWork\FrameWorkComponent\AlertPopup;
use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\Controller\BaseController;
use FrameWork\FrameWorkComponent\Controller\ControllerInterface;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\ManagerController;
use FrameWork\FrameWorkComponent\RenderFile;
use FrameWork\Repository\ArticleRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Mapping as ORM;
use FrameWork\FrameWorkComponent\Annotation\Worker;
use FrameWork\FrameWorkComponent\Annotation\WorkerInterface;
use FrameWork\FrameWorkComponent\Annotation\WorkerMethodController;

/**
 * Class ConnexionController
 * @package FrameWork\Controller
 */
class ConnexionController extends BaseController
{

    /**
     * @var array
     */
    private $url;

    /**
     * @var array
     */
    public static $link = [];

    /**
     * @var array
     */
    public static $assetsFile = [
        'js' => [],
        'css' => []
    ];

    /**
     * @var string
     */
    public static $titlePage = 'Connexion';

    /**
     * @var RenderFile
     */
    protected $response;

    /**
     * @var ArticleRepository
     */
    protected $repository;

    /**
     * HomeController constructor.
     * @param $url
     */
    public function __construct($url)
    {
        parent::__construct();
        $this->response = $this->getComponent('RenderFile');
        $this->repository = new ArticleRepository();

        $this->url = $url;

    }

    /**
     * @return RenderFile
     * @throws Fatal
     * @WorkerMethodController (
     *     name = "connexion",
     *     url = "/connexion"
     * )
     */
    public function viewConnexion(): RenderFile
    {

        $connexionFormType = new ConnexionFormType();
        $connexionFormType::setNameToForm('subscriptionConnexionForm');
        $connexionFormType->initializeForm();
        $connexionFormType->addTokenCsrf($connexionFormType::getNameForm());
        $initializeForm = $connexionFormType->getForm();
        $errorUserForm = false;
        $user = $this->getUser();
        $request = $this->getComponent('HttpRequest');
        if ($connexionFormType->isSubmitForm() && !$errorUserForm = $connexionFormType->isValid($initializeForm)) {

            $check = $this->checkCrediential($request->getPostData('email', true),$request->getPostData('password'));
            if(!$check) {
                $popup = $this->getComponent('AlertPopup');
                $popup->addAlert('error', 'Connexion', 'Votre email ou mot de passe est faux');
                return $request->redirection($this->getRoute('connexion'), 301);
            }

            return $request->redirection(getenv("PATH_APPLICATION"),200);

        }

        return $this->response->response([getenv('PATH_TEMPLATE') . 'connexion.php'],
            ['form' => $initializeForm, 'error' => $errorUserForm, 'request' => $request,'user' => $user]
            , $this);
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createUrl(ManagerController $managementController, string $controller)
    {
        $managementController->createUrl($controller, 'connexion', 'connexion');
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     * @return mixed|void
     */
    public static function createAssets(ManagerController $managementController, string $controller)
    {
        $managementController->createAssets($controller, 'connexion.css', 'css');
    }

    /**
     * @param string $routeName
     * @return mixed|string
     * @throws Fatal
     */
    public function getRoute(string $routeName)
    {
        try {
            foreach (self::$link as $route) {
                if ($routeName === $route['name']) {
                    return $route['routeName'];
                }
            }
        } catch (\Exception $exception) {
            throw new Fatal($exception);
        }
    }
}