<?php

namespace FrameWork\Controller;

use FrameWork\FrameWorkComponent\Controller\BaseController;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\ManagerController;
use FrameWork\FrameWorkComponent\RenderFile;
use FrameWork\Repository\ArticleRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Mapping as ORM;
use FrameWork\FrameWorkComponent\Annotation\Worker;
use FrameWork\FrameWorkComponent\Annotation\WorkerInterface;
use FrameWork\FrameWorkComponent\Annotation\WorkerMethodController;

/**
 * Class DeconnexionController
 * @package FrameWork\Controller
 */
class DeconnexionController extends BaseController {

    /**
     * @var array
     */
    private $url;

    /**
     * @var array
     */
    public static $link = [];

    /**
     * DeconnexionController constructor.
     * @param null $url
     */
    public function __construct($url = null)
    {
        parent::__construct();
        $this->url = $url;
    }

    /**
     * @return mixed
     * @throws \FrameWork\FrameWorkComponent\Error\ExceptionApp
     * @WorkerMethodController (
     *     name = "deconnexion",
     *     url = "/deconnexion"
     * )
     */
    public function deconnexion()
    {
        return parent::deconnexion();
    }

    public static function createUrl(ManagerController $managementController, string $controller)
    {
        // TODO: Implement createUrl() method.
    }

    public static function createAssets(ManagerController $managementController, string $controller)
    {
        $managementController->createUrl($controller, 'deconnexion', 'deconnexion');
    }

    public function getRoute(string $routeName)
    {
        // TODO: Implement getRoute() method.
    }
}