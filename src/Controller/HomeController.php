<?php

namespace FrameWork\Controller;

use Doctrine\Common\Annotations\Reader;
use FrameWork\FrameWorkComponent\Annotation\WorkerDiscovery;
use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\Controller\BaseController;
use FrameWork\FrameWorkComponent\Controller\ControllerInterface;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\ManagerController;
use FrameWork\FrameWorkComponent\RenderFile;
use FrameWork\Repository\ArticleRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Mapping as ORM;
use FrameWork\FrameWorkComponent\Annotation\Worker;
use FrameWork\FrameWorkComponent\Annotation\WorkerInterface;
use FrameWork\FrameWorkComponent\Annotation\WorkerMethodController;

/**
 * Class HomeController
 * @package FrameWork\Controller
 */
class HomeController extends BaseController
{
    /**
     * @var array
     */
    private $url;

    /**
     * @var array
     */
    public static $link = [];

    /**
     * @var array
     */
    public static $assetsFile = [
        'js' => [],
        'css' => []
    ];

    /**
     * @var string
     */
    public static $titlePage = 'Accueil';

    /**
     * @var RenderFile
     */
    protected $response;

    /**
     * @var ArticleRepository
     */
    protected $repository;

    /**
     * HomeController constructor.
     * @param null $url
     */
    public function __construct($url = null)
    {
        parent::__construct();
        $this->response = $this->getComponent('RenderFile');
        $this->repository = new ArticleRepository();

        $this->url = $url;

    }

    /**
     * @return RenderFile
     * @throws ExceptionApp
     * @WorkerMethodController (
     *     name = "home",
     *     url = "/home"
     * )
     */
    public function viewArticles(): RenderFile
    {
        $articles = new ArticleRepository;
        $user = $this->getUser();
        $articles = $articles->findAll();
        $this->response->response([getenv('PATH_TEMPLATE') . 'home.php'], ['articles' => $articles,'user' => $user], $this);
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createUrl(ManagerController $managementController, string $controller)
    {
        $managementController->createUrl($controller, 'home', 'home');
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createAssets(ManagerController $managementController, string $controller)
    {
        $managementController->createAssets($controller, 'home.css', 'css');
    }

    /**
     * @param string $routeName
     * @return mixed|string
     * @throws Fatal
     */
    public function getRoute(string $routeName)
    {

    }

}