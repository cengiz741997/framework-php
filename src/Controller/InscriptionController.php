<?php

namespace FrameWork\Controller;

use FrameWork\Entity\User;
use FrameWork\FormType\UserFormType;
use FrameWork\FrameWorkComponent\AlertPopup;
use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\Controller\BaseController;
use FrameWork\FrameWorkComponent\Controller\ControllerInterface;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\ManagerController;
use FrameWork\FrameWorkComponent\RenderFile;
use FrameWork\FrameWorkComponent\ScriptManagement\Entity\EntityInfo;
use FrameWork\Repository\ArticleRepository;
use FrameWork\FrameWorkComponent\UserEncoder\UserEncoder;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\ORM\Mapping as ORM;
use FrameWork\FrameWorkComponent\Annotation\Worker;
use FrameWork\FrameWorkComponent\Annotation\WorkerInterface;
use FrameWork\FrameWorkComponent\Annotation\WorkerMethodController;
use FrameWork\FrameWorkComponent\Annotation\WorkerParamController;

/**
 * Class InscriptionController
 * @package FrameWork\Controller
 */
class InscriptionController extends BaseController
{
    /**
     * @var array
     */
    private $url;

    /**
     * @var array
     */
    public static $link = [];

    /**
     * @var array
     */
    public static $assetsFile = [
        'js' => [],
        'css' => []
    ];

    /**
     * @var string
     */
    public static $titlePage = 'InscriptionController';

    /**
     * @var RenderFile
     */
    protected $response;

    /**
     * @var ArticleRepository
     */
    protected $repository;

    /**
     * InscriptionController constructor.
     * @param $url
     */
    public function __construct($url)
    {
        parent::__construct();
        $this->response = $this->getComponent('RenderFile');

        $this->url = $url;
    }

    /**
     * @return RenderFile
     * @throws Fatal
     * @WorkerMethodController (
     *     name = "subscriber",
     *     url = "/inscription"
     * )
     * @WorkerParamController (
     *     param = {
     *      "AlertPopup" : "$alertPopup",
     *      "RenderFile" : "$renderFile",
     *     }
     * )
     */
    public function viewInscription(AlertPopup $alertPopup,RenderFile $renderFile): RenderFile
    {
        $userFormType = new UserFormType();
        $userFormType::setNameToForm('subscriptionUserForm');
        $userFormType->initializeForm();
        $userFormType->addTokenCsrf($userFormType::getNameForm());
        $initializeForm = $userFormType->getForm();
        $errorUserForm = null;
        $request = $this->getComponent('HttpRequest');
        $user = $this->getUser();
        if ($userFormType->isSubmitForm() && !$errorUserForm = $userFormType->isValid($initializeForm)) {
            $entityManager = $this->getComponent('PersistData\EntityManager');
            // submit data to db
            $newUser = new User;
            $newUser->setName($request->getPostData('name', true));
            $newUser->setFirstname($request->getPostData('firstname', true));
            $newUser->setEmail($request->getPostData('email', true));
            $newUser->setCity($request->getPostData('city'));
            $password = UserEncoder::hashPassword($request->getPostData('password'));
            $newUser->setPassword($password);
            $newUser->setCp($request->getPostData('cp', true));
            $newUser->setCreatedAt((new \DateTime()));
            $newUser->addRoles(['ROLE_USER']);
            $entityManager->persist($newUser);
            $entityManager->flush();
            $popup = $this->getComponent('AlertPopup');
            $popup->addAlert('success', 'Inscription', 'Inscription réussi');
            $request->redirection($this->getRoute('inscription'), 301);
        }

        return $this->response->response([getenv('PATH_TEMPLATE') . 'inscription.php'],
            ['form' => $initializeForm, 'error' => $errorUserForm, 'request' => $request,'user' => $user]
            , $this);
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createUrl(ManagerController $managementController, string $controller)
    {
        $managementController->createUrl($controller, 'inscription', 'inscription');
    }

    /**
     * @param ManagerController $managementController
     * @param string $controller
     */
    public static function createAssets(ManagerController $managementController, string $controller)
    {
        $managementController->createAssets($controller, 'inscription.css', 'css');
    }

    /**
     * @param string $routeName
     * @return mixed|string
     * @throws Fatal
     */
    public function getRoute(string $routeName)
    {
        try {
            foreach (self::$link as $route) {
                if ($routeName === $route['name']) {
                    return $route['routeName'];
                }
            }
        } catch (\Exception $exception) {
            throw new Fatal($exception);
        }
    }

}