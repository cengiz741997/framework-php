<?php

namespace FrameWork\Entity;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\ORM\ManagerORM;

/**
 * Class Article
 * @package FrameWork\Entity
 */
class Article
{
    use ManagerORM;

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;

    /**
     * @var int
     */
    private $likes;

    /**
     * @var int
     */
    private $tags;

    /**
     * @var datetime
     */
    private $created_at;

    /**
     * @var User
     */
    private $user_id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int|null
     */
    public function getLikes(): mixte
    {
        return $this->likes;
    }

    /**
     * @param int|null $likes
     */
    public function setLikes($likes): void
    {
        $this->likes = $likes;
    }

    /**
     * @return int|null
     */
    public function getTags(): mixte
    {
        return $this->tags;
    }

    /**
     * @param int|null $tags
     */
    public function setTags($tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        $date = new \DateTime($this->created_at);
        return $date;
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt(string $created_at): void
    {
        $date = (new \DateTime($created_at))->format('d/m/Y H:i');
        $this->created_at = $date;
    }

    /**
     * @return User
     */
    public function getUserId(): User
    {
        return $this->user_id;
    }

    /**
     * @param User $user_id
     */
    public function setUserId(User $user_id): void
    {
        $this->user_id = $user_id;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * @param object $article
     * @param bool $optionORM
     * @throws ExceptionApp
     */
    public function hydrate(object $article, $optionORM = true)
    {
        foreach ($article as $key => $data) {

            // created_at
            $methode = 'set' . str_replace(' ', '', str_replace('_', '', ucfirst($key)));
            $isClass = ucfirst(substr($key, 0, strpos($key, "_")));

            // ici il nous faut regarder si une des keys présentes est représentée par une classe
            // par exemple chaque article à son créateur de base la donnée recu sera [ user id => 1] le souci c'est que nous voulons
            // l'objet user_id complet et n'ont une simple donnée comme sont id du coup on check avec la variable $isClass si celle ci existe
            // si elle existe nous devons récupérer toutes les données liées
            // nous avons après ceci un objet de données dans l'objet global lui-même

            // ex :
            // array (size=1)
            //  0 =>
            //    object(FrameWork\Entity\Article)[13]
            //          private 'id' => int 0
            //          .......
            //          .......
            //           private 'user_id' =>
            //               object(FrameWork\Entity\User)[17] <--- ici objet et non plus un simple id

            if (class_exists("FrameWork\Entity\\$isClass", true)) {

//              $class = "FrameWork\Entity\\$isClass";
                $classRepo = "FrameWork\Repository\\$isClass" . 'Repository';
                $className = $isClass;
                $repo = new $classRepo;
//                $relationClass = new $class;
                $repoMethodCall = 'findOneBy';
                $data = $repo->$repoMethodCall('id', $data);
                $this->$methode($data);
                if ($optionORM) {
                    $this->createObjectData($data);
                }

            } else {

                if (method_exists($this, $methode)) {
                    $this->$methode($data);
                } else {
                    throw new ExceptionApp("Méthode non existante , $methode", 1, 'Article.php', 168, '');
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getProperty()
    {
        return get_object_vars($this);
    }

}
