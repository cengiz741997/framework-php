<?php

namespace FrameWork\Entity;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;

/**
 * Class User
 * @package FrameWork\Entity
 */
class User
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $firstname;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $city;
    /**
     * @var string
     */
    private $cp;
    /**
     * @var string
     */
    private $password;
    /**
     * @var datetime
     */
    private $created_at;
    /**
     * @var array
     */
    private $articles = [];

    /**
     * @var array
     */
    private $roles = [];

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getCp(): string
    {
        return $this->cp;
    }

    /**
     * @param string $cp
     */
    public function setCp(string $cp): void
    {
        $this->cp = $cp;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt(\DateTime $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->firstname . ' ' . ucfirst($this->name);
    }

    /**
     * @return array
     */
    public function getArticles(): array
    {
        return $this->articles;
    }

    /**
     * @param Article $articles
     */
    public function setArticles(Article $articles): void
    {
        $this->articles[] = $articles;
    }

    /**
     * @param object $user
     * @throws ExceptionApp
     */
    public function hydrate(object $user)
    {

        foreach ($user as $key => $data) {

            // created_at
            $methode = 'set' . str_replace(' ', '', str_replace('_', '', ucfirst($key)));

            if (method_exists($this, $methode)) {
                $this->$methode($data);
            } else {
                throw new ExceptionApp("Méthode non existante , $methode", 1, 'User.php', 168, '');
            }
        }
    }

    /**
     * @return array
     */
    public function getProperty()
    {
        return get_object_vars($this);
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param string|array $role
     */
    public function addRoles($role)
    {
        if (!is_array($role)) {
            if (!in_array($role, $this->roles)) {
                $this->roles[] = $role;
            }
        } else {
            foreach ($role as $r) {
                if (!in_array($r, $this->roles)) {
                    $this->roles[] = $r;
                }
            }
        }
    }

    /**
     * @param string $role
     */
    public function removeRoles(string $role)
    {
        if (in_array($role, $this->roles)) {
            $roleDelete = array_search($role, $this->role); // key du rôle
            unset($this->roles[$roleDelete]);
            $this->roles = array_values($this->roles);
        }
    }

}