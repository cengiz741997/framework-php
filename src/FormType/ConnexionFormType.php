<?php

namespace FrameWork\FormType;

use FrameWork\FrameWorkComponent\FormType\BaseFormType;
use FrameWork\FrameWorkComponent\Regex\Regex;

/**
 * Class ConnexionFormType
 * @package FrameWork\FormType
 */
class ConnexionFormType extends BaseFormType
{

    public function initializeForm()
    {

        $this->form = [
            [
                'name' => 'email',
                'type' => 'email',
                'subject' => 'input',
                'title' => 'Email',
                'regex' => function ($value) {
                    return filter_var($value, FILTER_VALIDATE_EMAIL);
                },
                'options' => [
                    'required' => true,
                    'placeholder' => 'Veuillez entrer un email',
                    'class' => 'form-control'
                ]
            ],
            [
                'name' => 'password',
                'type' => 'password',
                'subject' => 'input',
                'title' => 'Mot de passe',
                'options' => [
                    'minLength' => 5,
                    'maxLength' => 30,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer un mot de passe',
                    'class' => 'form-control'
                ]
            ]
        ];

    }
}