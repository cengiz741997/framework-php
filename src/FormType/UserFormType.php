<?php

namespace FrameWork\FormType;

use FrameWork\FrameWorkComponent\FormType\BaseFormType;
use FrameWork\FrameWorkComponent\Regex\Regex;

/**
 * Class UserFormType
 * @package FrameWork\FormType
 */
class UserFormType extends BaseFormType
{

    /**
     * @return array[]|mixed
     * Pour chaque champ des vérifications seront faits automatiquement après soumission du formulaire
     * les check sont fait si la valeur existe, le pattern , minLength , maxLength , required
     * On peut appeler une reg ex ou bien la personnaliser en créant une méthode nous-même dans la key 'regex'
     *  *** Obligatoire ***
     *  name , type , subject
     *  *** Non Obligatoire ***
     *  title , pattern , minLength , maxLength , required , placeholder , class
     *
     */
    public function initializeForm()
    {

        $this->form = [
            [
                'name' => 'name',
                'type' => 'string',
                'subject' => 'input',
                'title' => 'nom',
                'regex' => Regex::STRING,
                'options' => [
                    'pattern' => Regex::PATTERN_HTML_STRING,
                    'minLength' => 2,
                    'maxLength' => 30,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer un nom',
                    'class' => 'form-control'
                ]
            ],
            [
                'name' => 'firstname',
                'type' => 'string',
                'subject' => 'input',
                'title' => 'prénom',
                'regex' => Regex::STRING,
                'options' => [
                    'pattern' => Regex::PATTERN_HTML_STRING,
                    'minLength' => 2,
                    'maxLength' => 30,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer un prénom',
                    'class' => 'form-control'
                ]
            ],
            [
                'name' => 'email',
                'type' => 'email',
                'subject' => 'input',
                'title' => 'email',
                'regex' => function ($value) {
                    return filter_var($value, FILTER_VALIDATE_EMAIL);
                },
                'options' => [
                    'minLength' => 4,
                    'maxLength' => 30,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer un email',
                    'class' => 'form-control'
                ]
            ],
            [
                'name' => 'password',
                'type' => 'password',
                'subject' => 'input',
                'title' => 'Mot de passe',
                'regex' => Regex::PASSWORD,
                'options' => [
                    'minLength' => 5,
                    'maxLength' => 30,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer un mot de passe',
                    'class' => 'form-control'
                ]
            ],
            [
                'name' => 'city',
                'type' => 'string',
                'subject' => 'input',
                'title' => 'ville',
                'regex' => Regex::CITY,
                'options' => [
                    'pattern' => Regex::PATTERN_HTML_CITY,
                    'minLength' => 2,
                    'maxLength' => 40,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer une ville',
                    'class' => 'form-control'
                ]
            ],
            [
                'name' => 'cp',
                'type' => 'number',
                'subject' => 'input',
                'title' => 'code postal',
                'regex' => Regex::POSTAL_CODE,
                'options' => [
                    'pattern' => Regex::PATTERN_HTML_POSTAL_CODE,
                    'minLength' => 5,
                    'maxLength' => 5,
                    'required' => true,
                    'placeholder' => 'Veuillez entrer votre code postal (fr)',
                    'class' => 'form-control'
                ]
            ],
        ];

        return $this->form;

    }

}
