<?php

namespace FrameWork\FrameWorkComponent;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;

/**
 * Class AlertPopup
 * @package FrameWork\FrameWorkComponent
 */
class AlertPopup
{

    /**
     * @var array
     * @info 2 type d'alerte
     * [ 'success' =>  [ ['title' => <title> , 'msg' => <msg>] , ...  ] ]
     * [ 'error' =>  [ ['title' => <title> , 'msg' => <msg>] , ...  ] ]
     * [ 'alert' =>  [ ['title' => <title> , 'msg' => <msg>] , ...  ] ]
     */
    public static $alert = [
        'success' => [],
        'error' => [],
        'warning' => []
    ];

    /**
     * @param string $type
     * @param string $title
     * @param string $msg
     * @throws ExceptionApp
     */
    public function addAlert(string $type, string $title, string $msg)
    {
        if (!array_key_exists($type, self::$alert)) {
            throw new ExceptionApp('Le type voulu d\'alert n\'exite pas (' . $type . ')',
                1,
                'AlertPopup.php',
                30,
                []
            );
        }

        self::$alert[$type][] = [
            'title' => $title,
            'msg' => $msg
        ];

        $_SESSION['alertPopup'] = self::$alert;
    }

    /**
     * @return false|mixed
     */
    public static function getAlert()
    {
        if (array_key_exists('alertPopup', $_SESSION)) {
            $alert = $_SESSION['alertPopup'];
            $_SESSION['alertPopup'] = [];
            return $alert;
        }
        return false;
    }

}