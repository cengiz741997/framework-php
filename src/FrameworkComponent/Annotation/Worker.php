<?php

namespace FrameWork\FrameWorkComponent\Annotation;

use Doctrine\Common\Annotations\Annotation;

// Exemple de worker pour en créer d'autre
/**
 * Class Worker
 * @Annotation
 * @Target("CLASS")
 * @package FrameWork\FrameWorkComponent\Controller\Annotations
 */
class Worker
{
    /**
     * @Required
     *
     * @var string
     */
    public $info;

    /**
     * @Required
     *
     * @var int
     */
    public $speed;

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return int
     */
    public function getSpeed()
    {
        return $this->speed;
    }
}