<?php

namespace FrameWork\FrameWorkComponent\Annotation;

use FrameWork\FrameWorkComponent\Annotation\Worker;
use Doctrine\Common\Annotations\Reader;
use FrameWork\FrameWorkComponent\Finder\Finder;

class WorkerDiscovery
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @var string
     */
    private $directory;

    /**
     * @var Reader
     */
    private $annotationReader;

    /**
     * The Kernel root directory
     * @var string
     */
    private $rootDir;

    /**
     * @var array
     */
    private $workers = [];

    /**
     * WorkerDiscovery constructor.
     *
     * @param $namespace
     *   The namespace of the workers
     *   The directory of the workers
     * @param $rootDir
     * @param Reader $annotationReader
     */
    public function __construct($namespace, $rootDir, Reader $annotationReader)
    {
        $this->namespace = $namespace;
        $this->annotationReader = $annotationReader;
        $this->rootDir = $rootDir;
    }

    /**
     * Returns all the workers
     */
    public function getWorkersController()
    {
        if (!$this->workers) {
            $find = $this->discoverWorkersController();
            return $find;
        }

        return $this->workers;
    }

    /**
     * @throws \ReflectionException
     */
    private function discoverWorkersController()
    {
        $finder = new Finder();
        $files = $finder->getFile($this->rootDir);
        /** @var SplFileInfo $file */
        foreach ($files as $file) {

            $class = $this->namespace . '\\' . $finder->getBaseName($this->rootDir . '/' . $file, true, '.php');
            //  on boucle sur tous les controllers jusqu'à trouver le contrôler qui détient une URL correspondante
            foreach (get_class_methods($class) as $method) {
                $requestUri = $_SERVER['REQUEST_URI'];
                $reflexion = new \ReflectionMethod($class, $method);
                $annotation = $this->annotationReader->getMethodAnnotations($reflexion, 'FrameWork\FrameWorkComponent\Annotation\WorkerMethodController');
                $createUrl = '';
                $parameters = [];

                if ($annotation) {
                    // on récupère l'url de l'annotation sur chaque méthode dans le controller et on check si on dispose d'une correspondance
                    $annotation = $annotation[0];
                    $urlController = $annotation->getUrl();
                    $explodeUrlController = array_values(array_filter(explode('/', $urlController)));
                    $explodeUrlUri = array_values(array_filter(explode('/', $requestUri)));

                    // on explode les 2 array si la taille est la même on check une correspondance si il ne dispose pas de la
                    // même taille on continue;
                    if (count($explodeUrlController) !== count($explodeUrlUri)) {
                        continue;
                    }

                    for ($i = 0; $i < count($explodeUrlUri); $i++) {
                        // ici on va construire notre url si on dispose tjrs de correspondance et aussi prendre en compte les paramètres de l'url style /voiture/{id}
                        if ($explodeUrlUri[$i] === $explodeUrlController[$i]) {
                            $createUrl .= $explodeUrlUri[$i] . '/';
                            continue;
                        }
                        // str_contains étant sur la version 8 de PHP on va tuiliser la technique de explode
                        // on va juste checker que count(explode('{' et '}',$explodeUrlController[$i])) >= 2
                        if (
                            count(explode('{', $explodeUrlController[$i])) >= 2
                            && count(explode('}', $explodeUrlController[$i])) >= 2
                        ) {
                            $createUrl .= $explodeUrlUri[$i] . '/';
                            $key = str_replace(['{','}'] , '' , $explodeUrlController[$i]);
                            $parameters[$key] = $explodeUrlUri[$i];

                        }
                    }

                }

                if ($createUrl) {
                    $lastCharacter = strlen($createUrl) - 1;
                    if ($createUrl[$lastCharacter] === '/') {
                        $createUrl = substr($createUrl, 0, -1);
                    }
                    return [
                        'url' => $createUrl,
                        'class' => $class,
                        'method' => $method,
                        'parameters' => $parameters
                    ];
                }
            }
            //            /** @var Worker $annotation */
            //            $this->workers[$annotation->getInfo()] = [
            //                'class' => $class,
            //                'annotation' => $annotation,
            //            ];
        }
        return false;
    }
}