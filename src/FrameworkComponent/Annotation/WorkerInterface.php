<?php

namespace FrameWork\FrameWorkComponent\Annotation;

interface WorkerInterface
{
    /**
     * Does the work
     *
     * @return NULL
     */
    public function work();
}