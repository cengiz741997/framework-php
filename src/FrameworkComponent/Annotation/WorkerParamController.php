<?php

namespace FrameWork\FrameWorkComponent\Annotation;

use Doctrine\Common\Annotations\Annotation;

/**
 * Class Worker
 * @Annotation
 * @package FrameWork\FrameWorkComponent\Controller\Annotations
 */
class WorkerParamController
{
    /**
     * @var array
     */
    public $param;

    /**
     * @return array
     */
    public function getParam(): array
    {
        return $this->param;
    }

    /**
     * @param array $param
     */
    public function setParam(array $param): void
    {
        $this->param = $param;
    }



}