<?php

namespace FrameWork\FrameWorkComponent;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;

/**
 * Class Application
 * @package FrameWork\FrameWorkComponent
 */
abstract class Application
{
    /**
     * @var static array
     */
    public static $component = [];

    /**
     * @param string $nameComponent
     * @return mixed
     */
    protected function getComponent(string $nameComponent)
    {
        $newComponent = 'FrameWork\FrameWorkComponent\\' . $nameComponent;

        if (class_exists($newComponent)) {
            if (!array_key_exists($nameComponent, self::$component)) {
                self::$component[$nameComponent] = new $newComponent();
            }

            return self::$component[$nameComponent];

        }

        $ex = new ExceptionApp('Cette classe n\'existe pas', 1, 'Application.php', 27, '');
        return $ex->callError();
    }

}