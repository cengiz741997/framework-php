<?php

namespace FrameWork\FrameWorkComponent\Controller;

use FrameWork\FrameWorkComponent\Annotation\WorkerManager;
use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Security\SecurityUser;
use FrameWork\FrameWorkComponent\UserEncoder\UserEncoder;
use FrameWork\FrameWorkComponent\Annotation\WorkerInterface;

/**
 * Class SlowWorker
 *
 * @Worker(
 *     name = "Slow Worker",
 *     speed = 5
 * )
 */
abstract class BaseController extends SecurityUser implements ControllerInterface , WorkerInterface {

    public function __construct() {

        $fileYaml = getenv('PATH_MAIN') . 'settings/settings.yaml';
        $yamlSettings = yaml_parse_file($fileYaml, 0);
        if (empty($active = $yamlSettings['settings']['middleware']['middlewareController']['active'])) {
            $error = new ExceptionApp(
                'Vous devez indiquer à true ou false le middlewar pour les controllers, voir fichier settings/settings.yaml',
                1,
                BaseController::class . '.php',
                16,
                []
            );
            $error->callError();
        }

        $middlewareController = $yamlSettings['settings']['middleware']['middlewareController'];
        if($active) {
           $middlewareDefault = new $middlewareController['defaultService']();
        }

    }

    /**
     * {@inheritdoc}
     */
    public function work()
    {
        return 'I work really slowly';
    }
}