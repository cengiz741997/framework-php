<?php

namespace FrameWork\FrameWorkComponent\Controller;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;
use FrameWork\FrameWorkComponent\ManagerController;

/**
 * Interface ControllerInterface
 * @package FrameWork\FrameWorkComponent\Controller
 */
interface ControllerInterface
{

    /**
     * @param ManagerController $managementController
     * @param string $controller
     * @return mixed
     */
    public static function createUrl(ManagerController $managementController, string $controller);

    /**
     * @param ManagerController $managementController
     * @param string $controller
     * @return mixed
     */
    public static function createAssets(ManagerController $managementController, string $controller);

    /**
     * @param string $routeName
     * @return mixed|string
     * @throws Fatal
     */
    public function getRoute(string $routeName);
}

