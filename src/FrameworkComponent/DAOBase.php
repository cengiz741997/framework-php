<?php

namespace FrameWork\FrameWorkComponent;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;

/**
 * Class DAOBase
 * @package FrameWork\FrameWorkComponent
 */
abstract class DAOBase
{
    /**
     * @var \Poo|null
     */
    protected static $__connection;

    /**
     * DAOBase constructor.
     * @throws Fatal
     */
    protected function __construct()
    {
        $dbUse = $_ENV['DB_USE'];

        try {
            switch ($dbUse) {
                case 'mysql':
                    $this->mySql();
                    return;

                case 'postgreSQL':
                    $this->postgreSQL();
                    return;

                default:
                    throw new \Exception(
                        "Aucune méthode alloué pour votre base de données '$dbUse', voir DAOBase.php"
                    );
            }
        } catch (\Exception $e) {
            throw new Fatal($e);
        }
    }

    /**
     * @return \PDO
     * @throws Fatal
     */
    protected function mySql()
    {
        if (self::$__connection != null) {
            return self::$__connection;
        }

        try {
            $database = getenv('DB_NAME');
            $host = getenv('DB_HOST');
            self::$__connection = new \PDO("mysql:host=$host;dbname=$database", getenv('DB_PSEUDO'), getenv('DB_PASSWORD'));
            return self::$__connection;
        } catch (PDOException $e) {
            throw new Fatal($e);
        }
    }

    /**
     *
     */
    protected function postgreSQL()
    {
        // ... connexion postgreSQL
    }

    /**
     * @return \PDO|null
     */
    protected function getPDO()
    {
        return self::$__connection;
    }
}