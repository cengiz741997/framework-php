<?php

namespace FrameWork\FrameWorkComponent\Error;

/**
 * Class ExceptionApp
 * @package FrameWork\FrameWorkComponent\Error
 */
class ExceptionApp extends \Exception
{

    const THROW_NONE = 0;
    const THROW_CUSTOM = 1;
    const THROW_DEFAULT = 2;

    /**
     * @var string
     */
    protected $message = "";
    /**
     * @var string
     */
    protected $severity = "";
    /**
     * @var string
     */
    protected $filename = "";
    /**
     * @var string
     */
    protected $line = "";
    /**
     * @var string|array
     */
    protected $trace = "";

    /**
     * @var string
     * @info Template d'erreur
     */
    protected $fileTemplate = "";

    /**
     * ExceptionApp constructor.
     * @param string $message
     * @param string $severity
     * @param string $filename
     * @param string $line
     * @param array $trace
     */
    public function __construct(string $message, string $severity, string $filename, string $line, array $trace)
    {
        $this->message = $message;
        $this->severity = $severity;
        $this->filename = $filename;
        $this->line = $line;
        $this->trace = $trace;
    }


    public function callError()
    {
        $this->fileTemplate = getenv('PATH_EXCEPTION_TEMPLATE');
        $data = [
            'message' => $this->message,
            'severity' => $this->severity,
            'filename' => $this->filename,
            'line' => $this->line,
            'trace' => $this->trace
        ];
        require_once($this->fileTemplate);
        die;
    }
}