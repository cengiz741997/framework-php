<?php

namespace FrameWork\FrameWorkComponent\Error;

class Fatal extends \Exception
{
    public $var;

    const THROW_NONE = 0;
    const THROW_CUSTOM = 1;
    const THROW_DEFAULT = 2;
    /**
     * @var string
     */
    protected $message = "";
    /**
     * @var string
     */
    protected $severity = "";
    /**
     * @var string
     */
    protected $filename = "";
    /**
     * @var string
     */
    protected $line = "";
    /**
     * @var string|array
     */
    protected $trace = "";

    /**
     * @var string
     * @info Template d'erreur
     */
    protected $fileTemplate = "";

    /**
     * Fatal constructor.
     * @param $contentErrorObjet
     */
    public function __construct(object $contentErrorObjet)
    {
        $this->message = $contentErrorObjet->getMessage();
        $this->severity = $contentErrorObjet->getCode();
        $this->file = $contentErrorObjet->getFile();
        $this->line = $contentErrorObjet->getLine();
        $this->trace = $contentErrorObjet->getTrace();

    }

    public function callError()
    {
        $this->fileTemplate = getenv('PATH_EXCEPTION_TEMPLATE');
        $data = [
            'message' => $this->message,
            'severity' => $this->severity,
            'filename' => $this->filename,
            'line' => $this->line,
            'trace' => $this->trace
        ];

        require_once($this->fileTemplate);
        die;
    }
}