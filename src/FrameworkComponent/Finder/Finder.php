<?php

namespace FrameWork\FrameWorkComponent\Finder;

/**
 * Class Finder
 * @package FrameWork\FrameWorkComponent\Finder
 */
class Finder {

    /**
     * @param string $dir
     * @param string $fileName
     * @return bool
     */
    public function fileExist(string $dir , string $fileName) {
        return file_exists($dir . '/' . $fileName);
    }

    /**
     * @param string $dir
     * @return array|false
     */
    public function getFile(string $dir ) {
        $allFiles = scandir($dir); // Or any other directory
        return array_diff($allFiles, array('.', '..'));
    }

    /**
     * @param string $dir
     * @param bool $extensionsWhiteout
     * @param string $extension
     * @return string
     */
    public function getBaseName(string $dir , bool $extensionsWhiteout = false , string $extension = '') {
        return $extensionsWhiteout ? basename($dir , $extension) : basename($dir);
    }

}
