<?php

namespace FrameWork\FrameWorkComponent\FormType;

use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\FormTypeMessageEvent\FormTypeMessageEvent;
use FrameWork\FrameWorkComponent\Regex\Regex;
use FrameWork\FrameWorkComponent\Security\TokenCsrf;

/**
 * Class BaseFormType
 * @package FrameWork\FrameWorkComponent\FormType
 */
abstract class BaseFormType extends Application
{
    /**
     * @var string
     */
    protected static $nameToForm;

    /**
     * @var array
     */
    protected $form;

    /**
     * @param string $nameFormType
     * @return string
     */
    public static function addNameToForm(): string
    {
        $name = self::$nameToForm;
        return "<input type='hidden' name='$name' >";
    }

    /**
     * @param string $nameToForm
     */
    public static function setNameToForm(string $nameToForm): void
    {
        self::$nameToForm = $nameToForm;
    }

    public function getForm() {
        return $this->form;
    }

    /**
     * @return bool
     */
    public function isSubmitForm(): bool
    {
        $request = $this->getComponent('HttpRequest');
        // check si request POST
        $isPost = $request->isRequestPost();
        // check si le formulaire est celui qui est concerné
        $isThisForm = isset($_POST[self::$nameToForm]);

        return $isPost && $isThisForm ? true : false;
    }

    /**
     * @param $name
     * @throws \Exception
     */
    public function addTokenCsrf($name) {
        $token = TokenCsrf::createTokenCsrf($name);
        $this->form[] = [
            'name' => '__csrf_token',
            'type' => 'text',
            'subject' => 'input',
            'title' => '',
            'options' => [
                'class' => 'd-none',
                'value' => $token
            ]
        ];
    }

    /**
     * @param array $currentFormSubmit
     * @return array|false
     */
    public function isValid(array $currentFormSubmit)
    {
        $errorForm = [];

        foreach ($currentFormSubmit as $field) {

            $nameField = $field['name'];
            $regex = !empty($field['regex']) ? $field['regex'] : null;
            $valueField = null;
            if (!isset($_POST[$nameField])) {
                $errorForm[$nameField]['msg'] = 'Le champ est manquant';
                continue;
            }

            $valueField = $_POST[$nameField];
            $minLength = !empty($field['options']['minLength']) ? $field['options']['minLength'] : null;
            $maxLength = !empty($field['options']['maxLength']) ? $field['options']['maxLength'] : null;
            $required = !empty($field['options']['required']) ? $field['options']['required'] : null;

            if ($required && strlen($valueField) === 0) {
                $errorForm[$nameField]['msg'] = 'Le champ est requis';
                $errorForm[$nameField]['value'] = $valueField;
                continue;
            }

            // ici cela concerne une une function ajouter dans le champ d'un formtype
            //            'name' => 'email',
            //                'type' => 'email',
            //                'subject' => 'input',
            //                'title' => 'email',
            //                'regex' => function ($value) { <-- exemple ceci
            //                return filter_var($value, FILTER_VALIDATE_EMAIL);
            //            },
            if (gettype($regex) === 'object' && !$regex($valueField)) {
                $errorForm[$nameField]['msg'] = 'Le champ contient une erreur';
                $errorForm[$nameField]['value'] = $valueField;
                continue;
            }

            // ici cela concerne une regex directement appeler par le biais de la class regex
            //            'name' => 'firstname',
            //                'type' => 'string',
            //                'subject' => 'input',
            //                'title' => 'prénom',
            //                'regex' => Regex::STRING <-- exemple ceci
            if (gettype($regex) === 'string' && !preg_match($regex, $valueField)) {
                // on check si on dispose d'un msg personnel ou non
                $errorMsg = !empty(FormTypeMessageEvent::EVENT_MESSAGE[$regex]) ? FormTypeMessageEvent::EVENT_MESSAGE[$regex]
                    : 'Le champ contient une erreur';
                $errorForm[$nameField]['msg'] = $errorMsg;
                $errorForm[$nameField]['value'] = $valueField;
                continue;
            }

            if ($minLength && strlen($valueField) < $minLength) {
                $errorForm[$nameField]['msg'] = 'Le champ est trop court';
                $errorForm[$nameField]['value'] = $valueField;
                continue;
            }
            if ($maxLength && strlen($valueField) > $maxLength) {
                $errorForm[$nameField]['msg'] = 'Le champ est trop long';
                $errorForm[$nameField]['value'] = $valueField;
                continue;
            }
        }

        if (empty($errorForm)) {
            // on check si on dispose ou non d'un jeton csrf à vérifier
            $sessionCsrf = $_SESSION['csrf'];
            if(array_key_exists(BaseFormType::$nameToForm , $sessionCsrf)) {
                // si un jeton est trouvé concernant le formulaire on test si les jetons les mêmes
                if(!TokenCsrf::checkTokenCsrf(BaseFormType::$nameToForm, $valueField)) {
                    $errorForm[$nameField]['msg'] = 'Erreur jeton Csrf';
                    $newCsrf = TokenCsrf::createTokenCsrf(BaseFormType::$nameToForm);
                    $errorForm[$nameField]['value'] = $newCsrf;
                    return $errorForm;
                }
            }
            return false;
        }

        return $errorForm;
    }

    /**
     * @return string
     */
    public static function getNameForm() {
        return self::$nameToForm;
    }

    /**
     * @param array $inputForm
     * @param false $defaultValue
     */
    public static function createForm(array $inputForm, $defaultValue = false)
    {
        $defaultValue = htmlspecialchars($defaultValue);
        // input , select , ... ?
        $subjectType = $inputForm['subject'];
        // options obligatoires
        $name = $inputForm['name'];
        $type = $inputForm['type'];

        // options non obligatoires
        $pattern = !empty($inputForm['options']['pattern']) ? $inputForm['options']['pattern'] : null;
        $minLength = !empty($inputForm['options']['minLength']) ? $inputForm['options']['minLength'] : null;
        $maxLength = !empty($inputForm['options']['maxLength']) ? $inputForm['options']['maxLength'] : null;
        $required = !empty($inputForm['options']['required']) ? $inputForm['options']['required'] : null;
        $placeholder = !empty($inputForm['options']['placeholder']) ? $inputForm['options']['placeholder'] : null;
        $class = !empty($inputForm['options']['class']) ? $inputForm['options']['class'] : null;
        $value = !empty($inputForm['options']['value']) ? $inputForm['options']['value'] : null;

        switch ($subjectType) {
            case 'input' :
                $input = "<input ";
                $type ? $input .= 'type="' . $type . '" ' : "";
                $name ? $input .= 'name="' . $name . '" ' : "";
                $value ?  $input .= 'value="' . $value . '" ' : "";
                // default value utilisé quand le formulaire submit n'est pas bon ( contient des erreurs )
                // on récup les données du formulaire et les affiches les données dans les inputs respectifs
                $defaultValue ? $input .= 'value="' . $defaultValue . '" ' : "";
                $placeholder ? $input .= 'placeholder="' . $placeholder . '" ' : "";
                $minLength ? $input .= 'minLength="' . $minLength . '" ' : "";
                $maxLength ? $input .= 'maxLength="' . $maxLength . '" ' : "";
                $class ? $input .= 'class="' . $class . '" ' : "";
                $pattern ? $input .= 'pattern="' . $pattern . '" ' : "";
                $required ? $input .= ' required' : "";
                $input .= "/>";
                echo $input;
                break;
        }
    }
}