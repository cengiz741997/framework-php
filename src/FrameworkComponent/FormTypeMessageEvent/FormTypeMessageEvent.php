<?php

namespace FrameWork\FrameWorkComponent\FormTypeMessageEvent;

use FrameWork\FrameWorkComponent\Regex\Regex;

/**
 * Class FormTypeMessageEvent
 * @package FrameWork\FrameWorkComponent\FormTypeMessageEvent
 */
class FormTypeMessageEvent
{

    const EVENT_MESSAGE = [
        Regex::STRING => 'Le champ contient une erreur',
        Regex::PASSWORD => 'Le mot de passe doit contenir une minuscule, une majuscule, un chiffre et les caractères spéciaux de',
        Regex::POSTAL_CODE => 'Le code postal contient une erreur'
    ];

}