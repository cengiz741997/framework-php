<?php

namespace FrameWork\FrameWorkComponent;

use FrameWork\FrameWorkComponent\Error\ExceptionApp;

/**
 * Class HttpRequest
 * @package FrameWork\FrameWorkComponent
 */
class HttpRequest
{
    /**
     * @var string[]
     */
    public $httpCode = [
        200 => '200 OK',
        301 => '301 Moved Permanently',
        403 => '403 Forbidden',
        404 => '404 Not Found'
        // .... ajouter vos code htpp ici
    ];

    /**
     * @return bool
     */
    public function isRequestPost()
    {
        return $_SERVER['REQUEST_METHOD'] === "POST" ? true : false;
    }

    /**
     * @param $namePostData
     * @return array|string|null|bool
     */
    public function getPostData($namePostData, bool $trim = false)
    {
        if(!empty($_POST[$namePostData])) {
            $data = htmlspecialchars($_POST[$namePostData]);
            if($trim) {
                $data = trim($data);
            }
            return $data;
        }
        return null;
    }

    /**
     * @param string $url
     * @param int $codeHtpp
     * @param bool $replace
     */
    public function redirection(string $url, int $codeHtpp, bool $replace = false)
    {
        if(!array_key_exists($codeHtpp , $this->httpCode)) {
            $ex = new ExceptionApp(
                "Veuillez ajouter votre code htpp dans l'attribut httpCode",
                1,
                HttpRequest::class . '.php',
                    49,
                []
            );
            return $ex->callError();
        }
        header("HTTP/1.1 $codeHtpp");
        header("Location:$url" , $replace);
        exit;
    }

}
