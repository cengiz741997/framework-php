<?php

namespace FrameWork\FrameWorkComponent;

/**
 * Class HttpResponse
 * @package FrameWork\FrameWorkComponent
 */
class HttpResponse extends Application
{

    /**
     * @var RenderFile
     */
    protected $response;

    public function __construct()
    {
        $this->response = $this->getComponent('renderFile');
    }

    public function call404()
    {
        $this->response->response([getenv('PATH_TEMPLATE') . 'base/404.php'], [], null);
    }

}