<?php

namespace FrameWork\FrameWorkComponent\InjectDep;

use Doctrine\Common\Annotations\AnnotationReader;
use FrameWork\Controller\InscriptionController;
use FrameWork\FrameWorkComponent\AlertPopup;
use FrameWork\FrameWorkComponent\Annotation\Worker;
use FrameWork\FrameWorkComponent\Annotation\WorkerManager;
use FrameWork\FrameWorkComponent\Annotation\WorkerParamController;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\RenderFile;

/**
 * Class InjectDep
 * @package FrameWork\FrameWorkComponent\InjectDep
 */
class InjectDep
{

    /**
     * @param string $class
     * @param string $method
     * @return array
     * @throws \ReflectionException
     */
    public function returnInformation(string $class, string $method): array
    {
        // création d'une reflection de methode d'une class
        $classReflection = new \ReflectionMethod($class,$method);
        $paramsClassReflection = $classReflection->getParameters();
        $paramMethodType = [];
        foreach ($paramsClassReflection as $param) {
            //$param is an instance of ReflectionParameter
            $paramMethodType[$param->getName()] = $param->getClass()->getName();
        }
        $annotationReader = new AnnotationReader();
        return [
            'classReflection' => $classReflection,
            'annotation' => $annotationReader->getMethodAnnotations($classReflection, WorkerParamController::class),
            'paramMethodType' => $paramMethodType
        ];
    }

    /**
     * @param array $annotationParams
     * @param array $paramMethodType
     * @return array|ExceptionApp
     */
    public function callDep(array $annotationParams , array $paramMethodType) {
        $dep = [];
        foreach ($annotationParams as $class => $param) {
            // $paramMethodType dispose des paramètres de la méthode voulue, elle dispose des full name des class
            // ici on va checker si le $param est présent dans le array $paramMethodType si oui on ajoute cette dep à $dep
            // si elle n'existe pas erreur
            $existParams = str_replace('$','',$param);
            if(!class_exists($paramMethodType[$existParams])) {
                return new ExceptionApp(
                    sprintf(
                        'La classe n\'existe pas %s',
                        $class
                    ),
                    1,
                    InjectDep::class,
                    32,
                    []
                );
            }

            if(!array_key_exists($existParams,$paramMethodType)) {
                return new ExceptionApp(
                    sprintf(
                        'Ajouter dans vos annotations de dépencances (WorkerParamController) de votre méthode cette dépencances %s',
                        $class
                    ),
                    1,
                    InjectDep::class,
                    32,
                    []
                );
            }

            $instanceOfClass = $paramMethodType[$existParams];
            array_push($dep,new $instanceOfClass);
        }

        return $dep;

    }


    public function injectDepMethodController(string $class, string $method)
    {
        $informationDep = $this->returnInformation($class,$method);
        $annotation = $informationDep['annotation'];
        $classReflection = $informationDep['classReflection'];
        $paramMethodType = $informationDep['paramMethodType'];
        // les deps de la méthode voulue
        $dependencies = [];

        foreach ($annotation as $anno) {
            // on récup les annotations voulues, donc "WorkerParamController"
            if ($anno instanceof WorkerParamController) {
                if (!array_key_exists('param', $anno)) {
                    return new ExceptionApp(
                        sprintf(
                            'Il manque la clée param dans votre Annotation %s du controller %s et pour méthode %s',
                            WorkerParamController::class,
                            $class,
                            $method
                        ),
                        1,
                        InjectDep::class,
                        32,
                        []
                    );
                }
                $dependencies = $this->callDep($anno->getParam(),$paramMethodType);
            }
        }

        return  [ 'dep' => $dependencies , 'refelctionClass' => $classReflection];
    }

}
