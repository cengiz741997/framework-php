<?php

namespace FrameWork\FrameWorkComponent;

/**
 * Class ManagerController
 * @package FrameWork\FrameWorkComponent
 */
class ManagerController
{

    /**
     * @param string $controller
     * @param string $nameRoute
     * @param string $route
     * @param null $parameters
     */
    public function createUrl(string $controller, string $nameRoute, string $route, $parameters = null)
    {
        if (!isset($controller::$link[$nameRoute])) {
            $controller::$link[$nameRoute] = [
                'name' => $nameRoute,
                'routeName' => getenv('PATH_APPLICATION') . $route,
                'parameters' => $parameters
            ];
        }
    }

    /**
     * @param string $controller
     * @param string $nameFileAssets
     * @param string $extensionFile
     */
    public function createAssets(string $controller, string $nameFileAssets, string $extensionFile)
    {

        if (isset($controller::$assetsFile[$extensionFile])) {
            $controller::$assetsFile[$extensionFile][] = $nameFileAssets;
        }

    }

    public function getRoute(string $nameRoute)
    {
        // TODO permet de récupérer n'importe qu'elle route de n'importe quelle contrôler
    }

}