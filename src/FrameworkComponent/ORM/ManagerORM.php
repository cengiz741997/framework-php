<?php

namespace FrameWork\FrameWorkComponent\ORM;

/**
 * Trait ManagerORM
 * @package FrameWork\FrameWorkComponent\ORM
 */
trait ManagerORM
{
    /**
     * @param object $classCurrent
     */
    public function createObjectData(object $classCurrent)
    {

        // Voici le fonctionnement
        // En premier lieu ont récup pour chaque classe ses attributs ensuite on recherche si l'un d'eux correspond
        // à une classe, ex : user id fait référence à la class user
        // il nous suffira de rechercher pour le Class concerné un attribut qui lui ressemble et ensuite de récupérer les données avec le repo

        $allClass = scandir($_ENV['PATH_ENTITY']);
        // on supprime le . et .. du scandir et on supprime aussi le current class qui la fait appelle
        $first = array_search('.', $allClass);
        unset($allClass[$first]);
        $second = array_search('..', $allClass);
        unset($allClass[$second]);

        // delete current class qui fait appelle à la methode
        $currentClassCall = substr(strrchr(get_class($classCurrent), "\\"), 1);
        $indexOfCurrentClass = array_search($currentClassCall . '.php', $allClass);
        unset($allClass[$indexOfCurrentClass]);
        $allClass = array_values($allClass);

        foreach ($allClass as $class) {
            // on delete le .php
            $callClass = str_replace('.php', '', $class);
            $attr = $classCurrent->getProperty();
            $relationSimple = array_key_exists(strtolower($callClass) . '_id', $attr);
            $relationMultiple = array_key_exists(strtolower($callClass) . 's', $attr);
            $relationNameSubject = null;

            $relationSimple ? $relationNameSubject = 'set' . strtolower($callClass) . '_id' : $relationNameSubject = 'set' . strtolower($callClass) . 's';

            $repositoryName = 'FrameWork\Repository\\' . $callClass . 'Repository';
            $repository = new $repositoryName;

            $getAllData = $repository->findBy(strtolower($currentClassCall . '_id'), $classCurrent);

            if ($relationSimple) {
                foreach ($getAllData as $data) {
                    $classCurrent->$relationNameSubject($data);
                }
            }

            if ($relationMultiple) {
                foreach ($getAllData as $data) {
                    $classCurrent->$relationNameSubject($data);
                }
            }
        }

    }

//    public function createObjectData()
//    {
//        // Voici le fonctionnement
//        // En premier lieu on récup pour chaque classe ses attributs ensuite on recherche si l'un deux correspond
//        // à une classe , ex : user_id fait référence à la class id
//        // il nous suffira de rechercher pour la class concerné un attributs qui lui ressemble
//
//        $allClass = scandir($_ENV['PATH_ENTITY']);
//        // on supprime le . et .. du scandir et on supprime aussi le current class qui la fait appelle
//        $first = array_search('.', $allClass);
//        unset($allClass[$first]);
//        $second = array_search('..', $allClass);
//        unset($allClass[$second]);
//
//        // delete current class qui fait appelle à la methode
//        $currentClassCall = substr(strrchr(__CLASS__, "\\"), 1);
//        $indexOfCurrentClass = array_search($currentClassCall . '.php', $allClass);
//        unset($allClass[$indexOfCurrentClass]);
//        $allClass = array_values($allClass);
//
//        foreach ($allClass as $class) {
//
//            // on delete le .php
//            $callClass = str_replace('.php', '', $class);
//            $class = 'FrameWork\Entity\\' . $callClass;
//            $class = new $class();
//            $attr = $class->getProperty();
//
//            $relationExists = array_key_exists(strtolower($callClass) . '_id', $attr);
//            var_dump($relationExists);
//
//
//        }
//
//    }

}