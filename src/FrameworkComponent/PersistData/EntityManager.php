<?php

namespace FrameWork\FrameWorkComponent\PersistData;

use FrameWork\FrameWorkComponent\DAOBase;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;

/**
 * Class EntityManager
 * @package FrameWork\FrameWorkComponent\PersistData
 */
class EntityManager extends DAOBase
{

    /**
     * @var \ArrayObject
     */
    protected $persistData;

    /**
     * @var string
     */
    protected $parameterQuery;

    /**
     * EntityManager constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->persistData = new \ArrayObject();
    }

    /**
     * @param object $data
     * @throws ExceptionApp
     */
    public function persist(object $data)
    {
        $this->persistData->append($data);
    }

    /**
     * @info Envoie les données dans la DB
     */
    public function flush()
    {

        foreach ($this->persistData as $data) {

            $reflectionClass = new \ReflectionClass(get_class($data));
            $arrayAttr = [];
            $dataPrepare = [];
            foreach ($reflectionClass->getProperties() as $property) {
                $property->setAccessible(true);
                if ($property->getName() != 'id') {
                    $arrayAttr[$property->getName()] = $property->getValue($data);
                    $dataPrepare[":" . $property->getName() . ""] = $property->getValue($data);
                }
            }

            $className = explode("\\", get_class($data));
            $className = strtolower($className[count($className) - 1]) . 's';

            $query = "INSERT INTO $className";
            $existColumn = null;

            $existColumn = $this->getPDO()->prepare("SHOW COLUMNS FROM $className");
            $existColumn->execute();
            $existColumn = $existColumn->fetchAll(\PDO::FETCH_COLUMN);

            if (in_array('id', $existColumn)) {
                $key = array_search('id', $existColumn);
                unset($existColumn[$key]);
                $existColumn = array_values($existColumn);
            }

            $lastAttr = end($existColumn);

            array_filter($existColumn, function ($attr) use ($lastAttr, $existColumn) {
                if ($attr === $lastAttr) {
                    $this->parameterQuery .= ":$attr";
                } else {
                    $this->parameterQuery .= ":$attr ,";
                }
            });

            $implodeExistColumn = implode(',', $existColumn);
            $query .= " ( $implodeExistColumn)";
            $query .= " VALUES (" . $this->parameterQuery . ")";

            foreach ($dataPrepare as $key => $column) {
                if (!in_array(str_replace(':', '', $key), $existColumn)) {
                    unset($dataPrepare[$key]);
                } else {
                    if (str_replace(':', '', $key) === 'id') {
                        unset($dataPrepare[$key]);
                        continue;
                    }
                    if (is_a($column, 'DateTime')) {
                        $dataPrepare[$key] = $column->format('Y/m/d H:i:s');
                        continue;
                    }
                    // TODO ajouter pour d'autre type exemple array json | simple ...
                    if (is_array($column)) {
                        $dataPrepare[$key] = json_encode($column);
                    }
                }
            }

            $request = $this->getPDO()->prepare(
                $query
            );
            $request->execute($dataPrepare);
        }
    }

}
