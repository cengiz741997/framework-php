<?php

namespace FrameWork\FrameWorkComponent\Regex;

final class Regex
{

    const STRING = "/^[A-Za-z]+$/";
    const CITY = "/^[A-Za-z -]+$/";
    const POSTAL_CODE = "/^[0-9]{5}$/";
    const PASSWORD = "/((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%?=*&]).{5,30})/";

    const PATTERN_HTML_STRING = "[A-Za-z]+";
    const PATTERN_HTML_CITY = "[A-Za-z -]+";
    const PATTERN_HTML_POSTAL_CODE = "[0-9]{5}";

}