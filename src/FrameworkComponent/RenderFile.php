<?php

namespace FrameWork\FrameWorkComponent;

use FrameWork\FrameWorkComponent\UrlLinkControllerManager;

/**
 * Class RenderFile
 * @package FrameWork\FrameWorkComponent
 */
class RenderFile
{
    /**
     * @param array $templates
     * @param array|null $parameters
     * @param object|null $controller
     */
    public function response(array $templates, array $parameters = null, ?object $controller)
    {

        // Voci le fonctionnement du rendu des pages en premier lieu on appelle le template base.php
        //  puis ont créer les données dans la variable $data qui va service pour le front, pour afficher des données
        //  ensuite la variable response elle l'ensemble du template rendu qui sera inseré dans base.php

        // ex : Rendu du template, voir fichier base.php la variable $content est présente dedans.
        // ---------------------
        // |      base.php      |
        // |   <$assetsContent> |
        // |                    |
        // |   ------------     |
        // |   | $content  |    |
        // |   |___________|    |
        // ---------------------|

        ob_start();

        // contient des données générales
        $data = [];
        $titlePage = '';
        $basePath = getenv("PATH_APPLICATION");
        $basePathAssets = getenv("PATH_APPLICATION_ASSETS");
        $basePathTemplate = getenv("PATH_TEMPLATE");
        $content = '';
        $assetsContent = '';
        // permet de récup les url de chaque controller pour les mettres dans des liens ( <a href='...'> )
        $managerUrl = UrlLinkControllerManager::class;
        $alertPopup = AlertPopup::class;
        // récupère les fichiers assets à charger pour le controlleur en question
        $assetsFiles = '';

        if ($controller) {
            $assetsFiles = $controller::$assetsFile;
            $titlePage = $controller::$titlePage;
        }

        if (!empty($parameters)) {
            foreach ($parameters as $key => $parameter) {
                $data[$key] = $parameter;
            }
        }

        foreach ($templates as $template) {
            require_once $template;
        }

        $content = ob_get_contents();
        ob_end_clean();
        // Initialize output container.
        require_once getenv('PATH_TEMPLATE') . 'base.php';

        $base = ob_get_contents();
        ob_end_clean();

        echo exit($base);

    }
}