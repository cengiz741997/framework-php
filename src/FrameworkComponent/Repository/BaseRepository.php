<?php

namespace FrameWork\FrameWorkComponent\Repository;

use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\DAOBase;
use FrameWork\FrameWorkComponent\PersistData\EntityManager;

/**
 * Class BaseRepository
 * @package FrameWork\Repository
 */
abstract class BaseRepository extends DAOBase implements BaseRepositoryInterface
{
    /**
     * @var EntityManager
     */
    protected static $entityManager;

    /**
     * BaseRepository constructor.
     */
    public function __construct()
    {
        parent::__construct();
        self::$entityManager = new EntityManager();
    }

}