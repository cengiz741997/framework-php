<?php

namespace FrameWork\FrameWorkComponent\Repository;

use FrameWork\FrameWorkComponent\DAOBase;

/**
 * Interface BaseInterface
 * @package FrameWork\Repository
 */
interface BaseRepositoryInterface
{

    /**
     * @return mixed
     */
    public function findAll();

    /**
     * @param string $attribut
     * @param $value
     * @return mixed
     */
    public function findOneBy(string $attribut, $value);

}