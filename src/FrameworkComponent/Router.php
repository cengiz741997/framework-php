<?php

namespace FrameWork\FrameWorkComponent;

use Doctrine\Common\Annotations\AnnotationReader;
use FrameWork\Controller\HomeController;
use FrameWork\Controller\InscriptionController;
use FrameWork\FrameWorkComponent\Annotation\WorkerDiscovery;
use FrameWork\FrameWorkComponent\Finder\Finder;
use Aura\Di\ContainerBuilder;
use FrameWork\FrameWorkComponent\InjectDep\InjectDep;

/**
 * Class Router
 * @package FrameWork\FrameWorkComponent
 */
class Router extends Application
{
    /**
     * @var array
     */
    protected $urlParameter = [
        "action" => "",
        "query" => ""
    ];

    /**
     * @param $url
     * @info Fonctionnement, la recherche d'un controlleur se fait via les annotations avec doctrine en utilisant la request URI
     *
     */
    public function invokeController()
    {
        $httpResponse = $this->getComponent('httpResponse');

        $this->initializeAllRouter();

        // check si l'uri est la même que getenv(PATH_APPLICATION) si oui on return direct le controller Home
        $protocol = stripos($_SERVER['SERVER_PROTOCOL'], 'https') === 0 ? 'https://' : 'http://';
        $host = $_SERVER['HTTP_HOST'];
        $uri = $_SERVER['REQUEST_URI'];

        if (getenv('PATH_APPLICATION') === $protocol . $host . $uri) {
            $homeController = new HomeController();
            return $homeController->viewArticles();
        }

        $annotation = new WorkerDiscovery('FrameWork\Controller', getenv('PATH_SRC') . 'Controller', new AnnotationReader());
        $findRoute = $annotation->getWorkersController();
        // on va checker tous les url de tous les controller pour voir si on trouve une correspondance

        if ($findRoute) {
            $builder = new ContainerBuilder();
            $controllerClass = $findRoute['class'];
            $methodCall = $findRoute['method'];
            $parameters = $findRoute['parameters'];

            // call injectDep
            $injectDep = new InjectDep;
            $injectDep = $injectDep->injectDepMethodController($controllerClass , $methodCall);
            $classReflection = $injectDep['refelctionClass'];
            return $classReflection->invoke(new $controllerClass($parameters) ,...$injectDep['dep']);
        }

        return $httpResponse->call404();

    }

    /**
     * @param array $arrayParameter
     */
    private function createUrlParameterArray(array $arrayParameter): void
    {
        isset($arrayParameter[0]) ? $this->urlParameter['action'] = htmlentities(str_replace(' ', '', $arrayParameter[0])) : $arrayParameter[0] = "";
        isset($arrayParameter[1]) ? $this->urlParameter['query'] = htmlentities(str_replace(' ', '', $arrayParameter[1])) : $arrayParameter[1] = "";
    }

    private function initializeAllRouter(): void
    {
        $dirControllers = __DIR__ . '\..\\Controller';
        $controllers = array_diff(scandir($dirControllers), array('..', '.'));
        $managerController = $this->getComponent('managerController');
        foreach ($controllers as $controller) {
            $controllerSubstr = substr($controller, 0, strpos($controller, "."));
            $controller = 'FrameWork\Controller\\' . $controllerSubstr;
            $controller::createUrl($managerController, $controller);
            $controller::createAssets($managerController, $controller);
        }
    }
}