<?php

namespace FrameWork\FrameWorkComponent\ScriptManagement\Entity;

$GLOBALS['listType'] = [
    'SMALLINT',
    'MEDIUMINT',
    'INT',
    'BIGINT',
    'DECIMAL',
    'FLOAT',
    'DOUBLE',
    'REAL',
    'BIT',
    'BOOLEAN',
    'SERIAL',
    'DATE',
    'DATETIME',
    'TIMESTAMP',
    'TIME',
    'YEAR',
    'CHAR',
    'VARCHAR',
    'TINYTEXT',
    'TEXT',
    'MEDIUMTEXT',
    'LONGTEXT',
    'BINARY',
    'VARBINARY',
    'TINYBLOB',
    'BLOB',
    'MEDIUMBLOB',
    'LONGBLOB',
    'ENUM',
    'SET',
    'GEOMETRY',
    'POINT',
    'LINESTRING',
    'POLYGON',
    'MULTIPOINT',
    'MULTILINESTRING',
    'MULTIPOLYGON',
    'GEOMETRYCOLLECTION',
    'JSON'
];

$GLOBALS['listTypeMembership'] = [
    'int' => [
        'SMALLINT',
        'MEDIUMINT',
        'INT',
        'BIGINT',
        'BIT',
        'SERIAL',
    ],
    'float' => [
        'DECIMAL',
        'FLOAT',
        'DOUBLE',
        'REAL'
    ],
    'bool' => [
        'BOOLEAN'
    ],
    'date' => [
        'DATE'
    ],
    'datetime' => [
        'TIMESTAMP',
        'TIME',
        'YEAR'
    ],
    'string' => [
        'CHAR',
        'VARCHAR',
        'TINYTEXT',
        'TEXT',
        'MEDIUMTEXT',
        'LONGTEXT',
        'TINYBLOB',
        'BLOB',
        'MEDIUMBLOB',
        'LONGBLOB',
    ],
    'array' => [
        'ENUM',
        'SET'
    ],
    'json' => [
        'JSON'
    ],
    'GEOMETRY' => [
        'GEOMETRY',
        'POINT',
        'LINESTRING',
        'POLYGON',
        'MULTIPOINT',
        'MULTILINESTRING',
        'MULTIPOLYGON',
        'GEOMETRYCOLLECTION',
    ]

];

echo "Création d'une nouvelle entité\r\n";

echo "Nom de la nouvelle entité : \r\n";

createEntity();

// nom de l'entité
$GLOBALS['entityName'] = '';

// nom de la current colonnes
$GLOBALS['column'] = '';
// attribut de celle ci globales
// ex : [ 'name' => 'years' , 'type' => int]
$GLOBALS['columns'] = [];

function createEntity()
{
    $stdinEntity = fopen('php://stdin', 'r');
    $responseEntity = trim(fgets(STDIN));
    $GLOBALS['entityName'] = $responseEntity;
    echo "Confirmer la nouvelle entité [Y-N] : \r\n";

    $stdinConfirmation = fopen('php://stdin', 'r');
    $responseConfirmation = fgetc($stdinConfirmation);

    if (strtoupper($responseConfirmation) == 'Y') {
        createAttribute();
    } else {
        echo "Annulation.\r\n";
        exit;
    }
}

function typeColumns()
{
    $column = $GLOBALS['column'];
    echo "\r\n Type pour la colonne  $column, [HELP](voir tous les types) :\r\n";
    $stdinAttr = fopen('php://stdin', 'r');
    $responseAttr = trim(fgets(STDIN));

    switch ($responseAttr) {
        case 'HELP':
            echo "DEBUT LISTE TYPES\r\n";
            print_r($GLOBALS['listType']);
            echo "END LISTE TYPES\r\n";
            typeColumns();
            break;
        default:
            if (!in_array(strtoupper($responseAttr), $GLOBALS['listType'])) {
                echo "\r\n \033[31m Le type voulu n'est pas présent dans la sélection :\033[0m\r\n";
                typeColumns();
            }
            $GLOBALS['columns'][] = ['name' => $GLOBALS['column'], 'type' => $responseAttr];
            echo "\r\n \033[32m Ajout de la column $column pour type  $responseAttr \033[0m\r\n";
            createAttribute();
            break;

    }
}

function createAttribute()
{
    $entity = $GLOBALS['entityName'];
    echo "\r\n Nom de la colonne pour l'entité $entity [STOP]\r\n";
    $stdinAttr = fopen('php://stdin', 'r');
    $responseAttr = trim(fgets(STDIN));

    switch (strtolower($responseAttr)) {
        case 'STOP':
        case 'stop':
            echo "\r\n Création de votre entité en cours \r\n";
            // todo création fichier new entity
            createNewEntityWorker();
            fclose($stdinAttr);
            break;
        default:
            $GLOBALS['column'] = $responseAttr;
            if ($responseAttr === '') {
                echo "\r\n \033[31m Veuillez indiquer un nom svp..\033[0m\r\n";
                createAttribute();
            }
            if (!preg_match("/^[A-Za-z]+$/", $responseAttr)) {
                echo "\r\n \033[31m Le nom de colonne choisi n'est pas au bon format :\033[0m\r\n";
                echo "\r\n \033[31m Format accepté exemple : ( created , createdat , createdAt , createdAT , CREATEDAT , CREATEDat ):\033[0m\r\n";
                createAttribute();
            }
            echo "\r\n Nom de colonne choisi \033[36m $responseAttr \033[0m \r\n";
            typeColumns();
            break;
    }

}

/**
 * @param string $type
 * @return bool|int|string
 */
function findMembershipColumn(string $type)
{
    foreach ($GLOBALS['listTypeMembership'] as $key => $listTypeMembership) {
        $checkember = in_array(strtoupper($type), $listTypeMembership);
        if ($checkember) {
            return $key;
        }
    }
    return $type;
}

function createNewEntityWorker()
{
    $dir = dirname(__FILE__);
    $dir = strstr($dir, 'FrameWork', true) . 'FrameWork\\src\\Entity\\';
    // Todo ajouter pour plus tard la modification d'un fichier déjà existant
    $file = $dir . ucfirst($GLOBALS['entityName']) . '.php';
    $existFile = file_exists($file);
    if ($existFile) {
        echo "\r\n \033[31m Cette class existe déjà l'implémentation de la modification n'est pas disponible pour l'instant dsl.\033[0m\r\n";
        return false;
    }

    EntityInfo::setNameEntity(ucfirst($GLOBALS['entityName']));
    EntityInfo::setPathFile($file);
    EntityInfo::setColumns($GLOBALS['columns']);
    EntityInfo::createEntity();

}

/**
 * Class EntityInfo
 * @package FrameWork\FrameWorkComponent\ScriptManagement\Entity
 */
class EntityInfo
{
    /**
     * @var string
     */
    private static $pathFile;
    /**
     * @var string
     */
    private static $nameEntity;
    /**
     * @var array
     */
    private static $columns;

    private const NAME_SPACE = "namespace FrameWork\Entity;";
    private const EXCEPTION_APP = "use FrameWork\FrameWorkComponent\Error\ExceptionApp;";
    private const MANAGER_ORM = "use FrameWork\FrameWorkComponent\ORM\ManagerORM;";

    /**
     * @var array CONST
     */
    private const HEADERS = [
        self::NAME_SPACE,
        self::EXCEPTION_APP,
        self::MANAGER_ORM
    ];

    public static function createEntity()
    {
        self::initilizeContent();
        self::createAnnotationClass();
        self::createColumns();
        self::createMethod();
        self::endEntity();
    }

    private static function createAnnotationClass()
    {
        $annotation = "/**" . PHP_EOL;
        $annotation .= " * Class " . self::$nameEntity . PHP_EOL;
        $annotation .= " * @package FrameWork\Entity" . PHP_EOL;
        $annotation .= "*/" . PHP_EOL;
        $annotation .= "class " . self::$nameEntity . PHP_EOL;
        $annotation .= "{" . PHP_EOL;
        file_put_contents(self::$pathFile, $annotation . PHP_EOL, FILE_APPEND);
    }

    private static function initilizeContent()
    {
        file_put_contents(self::$pathFile, "<?php" . PHP_EOL . PHP_EOL, FILE_APPEND);
        foreach (self::HEADERS as $header) {
            file_put_contents(self::$pathFile, $header . PHP_EOL, FILE_APPEND);
        }
    }

    private static function createColumns()
    {
        foreach (self::$columns as $column) {
            $columnContent = PHP_EOL . "    /**" . PHP_EOL;
            $type = findMembershipColumn($column['type']);
            $columnContent .= "     * @var " . $type . PHP_EOL;;
            $columnContent .= "    */" . PHP_EOL;
            $columnContent .= "    private $" . $column['name'] . ";" . PHP_EOL;
            file_put_contents(self::$pathFile, $columnContent, FILE_APPEND);
        }
    }

    private static function createMethod()
    {

        foreach (self::$columns as $column) {
            $type = findMembershipColumn($column['type']);
            $columnName = $column['name'];
            // setter //
            $columnContentSetter = PHP_EOL . "    /**" . PHP_EOL;
            $columnContentSetter .= "     * @param $type|null $" . $columnName . PHP_EOL;
            $columnContentSetter .= "    */" . PHP_EOL;
            $columnContentSetter .= "    public function set" . ucfirst($column['name']) . "($$columnName): void" . PHP_EOL;
            $columnContentSetter .= "    {" . PHP_EOL;
            $columnContentSetter .= '        $this->' . $columnName . " = $columnName;" . PHP_EOL;
            $columnContentSetter .= "    }" . PHP_EOL;
            // end setter //
            // getter //
            $columnContentGetter = PHP_EOL . "    /**" . PHP_EOL;
            $columnContentGetter .= "     * @return $type|null" . PHP_EOL;
            $columnContentGetter .= "    */" . PHP_EOL;
            $columnContentGetter .= "    public function get" . ucfirst($column['name']) . "(): mixte" . PHP_EOL;
            $columnContentGetter .= "    {" . PHP_EOL;
            $columnContentGetter .= '        return $this->' . $columnName . ';' . PHP_EOL;
            $columnContentGetter .= "    }" . PHP_EOL;
            // end getter //
            file_put_contents(self::$pathFile, $columnContentSetter, FILE_APPEND);
            file_put_contents(self::$pathFile, $columnContentGetter, FILE_APPEND);
        }
    }

    private static function endEntity()
    {
        $endEntity = '}';
        file_put_contents(self::$pathFile, $endEntity, FILE_APPEND);
    }

    /**
     * @param $path
     */
    public static function setPathFile($path)
    {
        self::$pathFile = $path;
    }

    /**
     * @param $entityName
     */
    public static function setNameEntity($entityName)
    {
        self::$nameEntity = $entityName;
    }

    /**
     * @param $columns
     */
    public static function setColumns($columns)
    {
        self::$columns = $columns;
    }


}


