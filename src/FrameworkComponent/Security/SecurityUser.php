<?php

namespace FrameWork\FrameWorkComponent\Security;

use FrameWork\FrameWorkComponent\Application;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\UserEncoder\UserEncoder;
use Firebase\JWT\JWT;

/**
 * Class SecurityUser
 * @package FrameWork\FrameWorkComponent\Security
 */
class SecurityUser extends Application
{

    /**
     * @param string $email
     * @param string $password
     * @return bool
     */
    public function checkCrediential(string $email , string $password): bool
    {

        // on récupère la class utiliséé pour la connection
        $fileYaml = getenv('PATH_MAIN') . 'settings/settings.yaml';
        $yamlSettings = yaml_parse_file($fileYaml, 0);

        if (empty($yamlSettings['settings']['login']['entity']['name'])) {
            $error = new ExceptionApp(
                'Vous devez ajouter l\'entité utilisé lors de la connection',
                1,
                SecurityUser::class . '.php',
                15,
                []
            );
            $error->callError();
        }
        $entity = $yamlSettings['settings']['login']['entity']['name'];
        // on va récup des les info de la db grâce au repo concernant l'entity utilisé lors de la connection
        // par default UserRepository.php
        $constructName = str_replace('Entity', 'Repository' , $entity);
        $repo = $constructName . 'Repository';
        if(!class_exists($repo , true)) {
            $error = new ExceptionApp(
                'Vous devez créer un repository du nom de ' . $repo,
                1,
                SecurityUser::class . '.php',
                33,
                []
            );
            $error->callError();
        }

        $repo = new $repo();
        $user = $repo->findOneBy('email' , $email);
        if(!$user) {
            return false;
        }

        $checkPassword = password_verify($password , $user->getPassword());

        if(!$checkPassword) {
            return false;
        }

        $userPayload = [
            'id' => $user->getId(),
            'name' => $user->__toString(),
            'role' => $user->getRoles()
        ];

        $jwt = JWT::encode($userPayload,getenv('SECRET_TOKEN'));
        $_SESSION['user'] = $jwt;
        return true;
    }

    /**
     * @return false|object
     */
    public function getUser() {
        if(empty($_SESSION['user'])) {
            return false;
        }

       return JWT::decode($_SESSION['user'],getenv('SECRET_TOKEN'),array('HS256'));

    }

    public function deconnexion() {

        if(isset($_SESSION['user'])) {
            unset($_SESSION['user']);
        }

        $httpComponent = $this->getComponent('HttpRequest');
        return $httpComponent->redirection(getenv('PATH_APPLICATION') , 200);
    }

}
