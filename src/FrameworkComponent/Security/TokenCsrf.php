<?php

namespace FrameWork\FrameWorkComponent\Security;

/**
 * Class TokenCsrf
 * @package FrameWork\FrameWorkComponent\Security
 * @info le jeton csrf marche de la sorte
 *
 */
class TokenCsrf
{

    /**
     * @param string $name
     * @return string
     * @throws \Exception
     */
    public static function createTokenCsrf(string $name): string
    {
        $token = bin2hex(random_bytes(32));
        if(!isset($_SESSION['csrf'])) {
            $_SESSION['csrf'] = [];
        }
        if(!array_key_exists($name,$_SESSION['csrf'])){
            $_SESSION['csrf'][$name] = [
                'token' => $token,
                'check' => false
            ];
            return $token;
        }
        if(!$_SESSION['csrf'][$name]['check'])  {
            return $_SESSION['csrf'][$name]['token'];
        }

        return $token;
    }

    public static function cleanTokenCsrf(): void
    {
        if (isset($_SESSION['csrf'])) {
            unset($_SESSION['csrf']);
        }
    }

    /**
     * @param string $name
     * @param string $token
     * @return bool
     */
    public static function checkTokenCsrf(string $name, string $token)
    {
        // aucune session csrf
        if (!isset($_SESSION['csrf'])) {
            return false;
        }
        // le nom du formulaire contenant le jeton n'exist pas
        if (!isset($_SESSION['csrf'][$name])) {
            return false;
        }

        if($_SESSION['csrf'][$name]['check']) {
            return false;
        }
        // jeton invalide ou non
        $sessionTokenCsrf = $_SESSION['csrf'][$name]['token'];
        return $sessionTokenCsrf === $token;

    }

}