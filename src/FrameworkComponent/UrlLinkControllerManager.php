<?php

namespace FrameWork\FrameWorkComponent;

use Doctrine\Common\Annotations\AnnotationReader;
use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Finder\Finder;

/**
 * Class UrlLinkControllerManager
 * @package FrameWork\FrameWorkComponent
 */
class UrlLinkControllerManager
{

    /**
     * @param string $nameRouteWant
     * @param array $parameters
     * @return ExceptionApp|string
     * @throws \ReflectionException
     */
    public static function getUrl(string $nameRouteWant, array $parameters = [])
    {
        $finder = new Finder();
        $files = $finder->getFile(getenv('PATH_SRC') . '/Controller');
        /** @var SplFileInfo $file */

        foreach ($files as $file) {

            $class = 'FrameWork\Controller\\' . $finder->getBaseName($file, true, '.php');
            //  on boucle sur tous les controllers jusqu'à trouver le contrôler qui détient une URL correspondante
            foreach (get_class_methods($class) as $method) {

                $reflexion = new \ReflectionMethod($class, $method);
                $annotationReader = new AnnotationReader();
                $annotation = $annotationReader->getMethodAnnotations($reflexion, 'FrameWork\FrameWorkComponent\Annotation\WorkerMethodController');

                $createUrl = '';
                if ($annotation) {

                    $annotation = $annotation[0];
                    $createUrl = $annotation->getUrl();
                    // on check si le name est le même que celui demandé
                    if($annotation->getName() === $nameRouteWant) {
                        foreach ($parameters as $key => $parameter) {
                            $key = '{' . $key . '}';
                            $createUrl = str_replace($key,$parameter,$createUrl);
                        }
                        return $createUrl;
                    }
                }
            }
        }
        return new ExceptionApp(
            'Aucune route existante pour ' . $nameRouteWant,
            1,
            UrlLinkControllerManager::class,
            48,
            []
        );
    }

}