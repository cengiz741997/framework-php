<?php

namespace FrameWork\FrameWorkComponent\UserEncoder;


use FrameWork\FrameWorkComponent\Error\ExceptionApp;
use FrameWork\FrameWorkComponent\Error\Fatal;

/**
 * Class UserEncoder
 * @package FrameWork\FrameWorkComponent\UserEncoder
 */
class UserEncoder
{
    /**
     * @var
     */
    private static $hash;

    private static function getEncodeHash(): void
    {
        $fileYaml = getenv('PATH_MAIN') . 'settings/settings.yaml';
        $yamlSettings = yaml_parse_file($fileYaml, 0);
        if (empty($yamlSettings['settings']['encoders']['algo'])) {
            $error = new ExceptionApp(
                'Vous devez indiquer une algo de hashage de mot de passe voir fichier settings/settings.yaml',
                1,
                        UserEncoder::class . '.php',
                28,
                []
            );
            $error->callError();
        }
        $hash = $yamlSettings['settings']['encoders']['algo'];
        $algo = "PASSWORD_" . strtoupper($hash);

        // test du hash return un warning sil le hash n'existe pas
        password_hash('test hash', constant($algo));

        self::$hash = constant($algo);
    }

    /**
     * @param string $password
     * @param array $options
     * @return false|string|null
     */
    public static function hashPassword(string $password , array $options = [])
    {
        if(!self::$hash) {
            self::getEncodeHash();
        }
        return password_hash($password , self::$hash, $options);
    }


}