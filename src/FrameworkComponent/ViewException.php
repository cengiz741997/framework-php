<script>
    let bodyApp = document.getElementsByTagName('body')[0];
    bodyApp.remove();
</script>

<style>

    body {
        margin: 0px;
        padding: 0px;
        font-family: Arial;
    }

    .error-mode {
        background-color: red;
        height: 100vh;
        padding: 2em;
    }

    .content-error {
        background-color: white;
        padding: 1em;
        margin: 2em;
        border-radius: 1em;
        word-wrap: break-word;
    }

    .text-decoration {
        text-decoration: underline;
    }

    .margin {
        margin: 1em;
    }

</style>

<div class='error-mode'>
    <div class='text-decoration'>
        <h2>Une Erreur est survenue : </h2>
    </div>
    <div class='content-error'>
        <h2>
            <p class='text-decoration'>Message : </p>
            <?= $data['message'] ?>
        </h2>
        <h2>
            <p class='text-decoration'>Severity : </p>
            <?= $data['severity'] ?>
        </h2>
        <h2>
            <p class='text-decoration'>Filename : </p>
            <?= $data['filename'] ?>
        </h2>
        <h2>
            <p class='text-decoration'>Line : </p>
            <?= $data['line'] ?>
        </h2>
        <h2>
            <p class='text-decoration'>Trace : </p>
            <ul>
                <?php
                isset($data['trace'][0]) ? $data['trace'] = $data['trace'][0] : $data['trace'];
                foreach ($data['trace'] as $key => $value) {
                    if (is_array($value)) {
                        echo '<div class="margin">
                                <p class="text-decoration">' . $key . '</p>';
                        echo '<ul>';
                        foreach ($value as $keyValue => $val) {
                            echo '<li>' . $keyValue . ' : ' . $val . '</li>';
                        }
                        echo '</ul></div>';
                    } else {
                        echo '<li>' . $key . ' : ' . $value . '</li>';
                    }
                }
                ?>
            </ul>
        </h2>
    </div>
</div>
