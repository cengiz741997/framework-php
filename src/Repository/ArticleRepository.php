<?php

namespace FrameWork\Repository;

use FrameWork\Entity\Article;
use FrameWork\FrameWorkComponent\DAOBase;
use FrameWork\FrameWorkComponent\Repository\BaseRepository;

/**
 * Class ArticleRepository
 * @package FrameWork\Repository
 */
class ArticleRepository extends BaseRepository
{

    /**
     * ArticleRepository constructor.
     * @throws \FrameWork\FrameWorkComponent\Error\Fatal
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return array
     * @throws \FrameWork\FrameWorkComponent\Error\ExceptionApp
     */
    public function findAll()
    {
        $request = $this->getPDO()->prepare('SELECT * from articles');

        $request->execute();
        $data = $request->fetchAll(\PDO::FETCH_OBJ);

        $articles = [];

        foreach ($data as $article) {

            $smallDescription = strlen($article->description);

            if ($smallDescription > 350) {
                $article->description = substr($article->description, 0, 150);
                $article->description .= '...';
            }

            $newArticle = new Article;
            $newArticle->hydrate($article);
            $articles[] = $newArticle;
        }

        return $articles;
    }

    /**
     * @param string $attribut
     * @param object $value
     * @return array
     * @throws \FrameWork\FrameWorkComponent\Error\ExceptionApp
     */
    public function findBy(string $attribut, object $value)
    {

        $sql = "SELECT * from articles WHERE $attribut = :value";
        $request = $this->getPDO()->prepare($sql);

        $request->execute([
            'value' => $value->getId()
        ]);

        $articles = $request->fetchAll(\PDO::FETCH_OBJ);

        $newArticles = [];

        if (!$articles) {
            return false;
        }

        foreach ($articles as $article) {
            $newArticle = new Article;
            $newArticle->hydrate($article, false);
            $newArticles[] = $newArticle;
        }

        return $newArticles;
    }

    /**
     * @param string $attribut
     * @param $value
     * @return false|Article|mixed
     * @throws \FrameWork\FrameWorkComponent\Error\ExceptionApp
     */
    public function findOneBy(string $attribut, $value)
    {
        $sql = "SELECT * from articles WHERE $attribut = ";

        $request = $this->getPDO()->prepare("
           $sql :value
        ");

        $request->execute([
            'value' => $value
        ]);

        $newArticle = new Article;

        $article = $request->fetchObject();

        if (!$article) {
            return false;
        }

        $newArticle->hydrate($article);

        return $newArticle;

    }
}
