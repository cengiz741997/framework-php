<?php

namespace FrameWork\Repository;

use FrameWork\Entity\Article;
use FrameWork\Entity\User;
use FrameWork\FrameWorkComponent\DAOBase;
use FrameWork\FrameWorkComponent\Repository\BaseRepository;

/**
 * Class UserRepository
 * @package FrameWork\Repository
 */
class UserRepository extends BaseRepository
{

    /**
     * UserRepository constructor.
     * @throws \FrameWork\FrameWorkComponent\Error\Fatal
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function findAll()
    {
        // TODO: Implement getAll() method.
    }

    /**
     * @param string $attribut
     * @param $value
     * @return mixed
     */
    public function findOneBy(string $attribut, $value)
    {
        $request = $this->getPDO()->prepare("SELECT * FROM users WHERE $attribut = :value ");
        $request->execute(array(':value' => $value));
        $data = $request->fetchObject('FrameWork\Entity\User');

        return $data;
    }
}
