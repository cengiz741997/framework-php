<?php

$alert = $alertPopup::getAlert();
$success = null;
$error = null;
$warning = null;

if ($alert) {
    $success = $alert['success'];
    $error = $alert['error'];
    $warning = $alert['warning'];
}

?>

<div class="content-alert m-2 p-1">

    <div class="content-success">
        <?php
        if ($success) {
            foreach ($success as $alert) { ?>
                <div class="d-flex justify-content-center">
                    <div class="alert alert-success w-50">
                        <h6><?php echo $alert['title'] ?></h6>
                        <p><?php echo $alert['msg'] ?></p>
                    </div>
                </div>
            <?php }
        }
        ?>
    </div>

    <div class="content-alert">
        <?php
        if ($error) {
            foreach ($error as $alert) { ?>
                <div class="d-flex justify-content-center">
                    <div class="alert alert-danger">
                        <h6><?php echo $alert['title'] ?></h6>
                        <p><?php echo $alert['msg'] ?></p>
                    </div>
                </div>
            <?php }
        }
        ?>
    </div>

    <div class="content-error">
        <?php
        if ($warning) {
            foreach ($warning as $alert) { ?>
                <div class="d-flex justify-content-center">
                    <div class="alert alert-warning">
                        <h6><?php echo $alert['title'] ?></h6>
                        <p><?php echo $alert['msg'] ?></p>
                    </div>
                </div>
            <?php }
        }
        ?>
    </div>

</div>
