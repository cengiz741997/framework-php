<?php

$article = $data['article'];
$author = $article->getUserId();

?>

<br/><br/>
<info class="m-2"> > <em>Article numéro <?php echo htmlentities($article->getId()); ?></em></info>

<div class="card container content-article front">
    <div class="content-title-article m-1">
        <h3 class="text-center title-article"><?php echo htmlentities($article->getTitle()); ?></h3>
    </div>
    <div class="image-article-content">
        <img class="image-article"
             src="https://www.challenges.fr/assets/img/2018/08/27/cover-r4x3w1000-5b84072224873-pbc18-conference-09-jpg.jpg">
    </div>
    <div class="text-article-content">
        <p class="text-article">
            <?php echo htmlentities($article->getDescription()); ?>
        </p>
    </div>
    <div class="footer-article-content text-article-content">
        <em>Information :</em>
        <hr/>
        <p>
            Auteur :
            <?php
            echo htmlentities(ucfirst($author->getName())) . ' ' . htmlentities(ucfirst($author->getFirstName()));
            ?>
        </p>
        <p>
            Date de publication :
            <?php
            echo htmlentities($article->getCreatedAt()->format('d/m/Y'));
            ?>
        </p>
        <p>
            Total articles écrit :
            <?php
            echo htmlentities(count($author->getArticles()));
            ?>
        </p>
    </div>
    <br/>
</div>

