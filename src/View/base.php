<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo $titlePage ?></title>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
            integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
            crossorigin="anonymous"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Pacifico&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
    <link rel="stylesheet" href="<?php echo $basePathAssets ?>css/fontawesome/css/all.min.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <?php require 'base/assets.css.php';  ?>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light shadow-lg p-3 bg-white text-dark nav">
    <div class="d-flex justify-content-start">
        <a class="navbar-brand" href="#">FrameWork </a>
        <img class="logo-php" src="<?php echo $basePathAssets ?>image/php.png">
    </div>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse d-flex justify-content-end" id="navbarNav">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link home-button" href="<?php echo $managerUrl::getUrl('home') ?>">Home
                    <span
                            class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <a class="nav-link hover-nav-a"
                   href="<?php echo $managerUrl::getUrl('subscriber') ?>">Inscription</a>
            </li>
            <li class="nav-item active">
                <?php
                include_once 'base/userLogged.php';
                ?>
            </li>
        </ul>
    </div>
</nav>
<div id="content-app" class="content">
    <img class="wave-ui" src="<?php echo $basePathAssets ?>image/wave-ui.png"/>
    <div class="main-app">
        <?php echo $content; ?>
    </div>
</div>

>
<?php require 'base/assets.js.php';  ?>

</body>
</html>