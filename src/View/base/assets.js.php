<?php
$baseJs = $basePathAssets . 'js/app/navbar.js';
echo "<script src='$baseJs'></script>";

if ($assetsFiles) {
    foreach ($assetsFiles as $extension => $files) {
        foreach ($files as $file) {
            switch ($extension) {
                case 'js':
                    echo "<script src='$basePathAssets$extension/$file'></script>".PHP_EOL;
                    break;
            }
        }
    }
}

?>