<?php
$user = !empty($data['user']) ? $data['user'] : null;
?>


<ul class="user-menu">
    <li class="nav-item -mouse-pointer li-no-style">
        <div class="user-display-menu">
            <i class="far fa-user"></i>
            <div class="menu-scroll-user shadow-lg p-3 bg-white rounded no-op">
                <?php
                if ($user) { ?>
                    <p class="text-dark">Bienvenue <?php echo $user->name ?></p>
                    <a class="nav-link nav-link hover-nav-a"
                       href="<?php echo $managerUrl::getUrl('deconnexion') ?>">Deconnexion</a>
                <?php } else { ?>
                    <a class="nav-link nav-link hover-nav-a"
                       href="<?php echo $managerUrl::getUrl('connexion') ?>">Connexion</a>
                <?php } ?>
            </div>
        </div>
    </li>
</ul>
<!--        <div class="bar-user-logged p-1 d-flex justify-content-between">-->
<!--            <p class="welcome-user">-->
<!--                Bienvenue --><?php //echo $user->name ?>
<!--            </p><i class="far fa-user"></i>-->
<!--            <a class="btn deco-btn"-->
<!--               href="--><?php //echo $managerUrl::getUrl('deconnexion', 'deconnexion'); ?><!--">Déconnexion</a>-->
<!--        </div>-->

