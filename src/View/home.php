<?php
$articles = $data['articles'];
?>
<h2 class="title-articles text-center front">Nos Articles </h2>
<hr class="line-small front">

<div>
    <div class="m-3 p-3 content-articles d-flex justify-content-center flex-wrap front">

        <ul>
            <?php

            foreach ($articles as $article) {
                $idArticle = $article->getId();
                $author = $article->getUserId();
                ?>
                <li>
                    <div class="card m-3" style="width: 18rem;">
                        <img class="card-img-top"
                             src="https://www.challenges.fr/assets/img/2018/08/27/cover-r4x3w1000-5b84072224873-pbc18-conference-09-jpg.jpg"
                             alt="Card image cap">
                        <div class="card-body">
                            <h5 class="card-title"><?php echo htmlentities($article->getTitle()); ?></h5>
                            <p class="card-text"><?php echo htmlentities($article->getDescription()); ?></p>
                            <a class="nav-link btn btn-primary"
                               href="<?php echo $managerUrl::getUrl('viewOneArticle', ['id' => $idArticle]); ?>">Voir
                                l'article</a>
                        </div>
                        <div class="card-footer">
                            <p>
                                <i class="far fa-user"></i>&nbsp;&nbsp;<?php echo htmlentities(ucfirst($author->getFirstName())) . ' ' . htmlentities(ucfirst($author->getName())); ?>
                            </p>
                            <p>
                                <i class="far fa-clock"></i>&nbsp;&nbsp;<?php echo htmlentities($article->getCreatedAt()->format('d/m/Y')); ?>
                            </p>
                        </div>
                    </div>
                </li>
                <?php
            }
            ?>
        </ul>

    </div>

    <div class="framework-section w-100">
        <h2 class="advisor">FrameWork Php By Kurt Cengiz( not finished &#128540; )</h2>
        <div class="content-framework-section d-flex align-items-center justify-content-center">
            <img class="w-100 position-absolute wave-ui-2" src="<?php echo $basePathAssets ?>image/wave-ui2.png"/>
            <div class="icons-framework-section position-absolute d-flex align-items-center justify-content-center">
                <div class="card-section shadow-lg m-2">
                    <i class="fas fa-address-book icon-framework"></i>
                    <p class="title-icon-framework">Model</p>
                </div>
                <div class="card-section shadow-lg m-2">
                    <i class="far fa-copy icon-framework"></i>
                    <p class="title-icon-framework">View</p>
                </div>
                <div class="card-section shadow-lg m-2">
                    <i class="fas fa-cogs icon-framework"></i>
                    <p class="title-icon-framework">Controller</p>
                </div>
            </div>
        </div>
    </div>
</div>