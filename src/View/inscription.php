<?php
$form = $data['form'];
$request = $data['request'];
?>
<h2 class="title-articles text-center front">Inscription</h2>
<hr class="line-small front">
<?php include_once($basePathTemplate . 'alertPopup/alertPopup.php') ?>
<div class="p-2 front">
    <div class="m-auto div-content-form p-3 shadow bg-white">
        <form action="#" method="post">
            <?php
            echo \FrameWork\FormType\UserFormType::addNameToForm();
            foreach ($form as $subject) {
                $error = null;
                $error = isset($data['error'][$subject['name']]) ? $data['error'][$subject['name']]['msg'] : null;
                $defaultValue = null;
                $error ? $defaultValue = $data['error'][$subject['name']]['value'] : $defaultValue = $request->getPostData($subject['name']);
                ?>
                <div class="form-group">
                    <label for="exampleInputEmail1"><?php echo ucfirst($subject['title']) ?></label>
                    <?php echo \FrameWork\FormType\UserFormType::createForm($subject, $defaultValue); ?>
                    <small id="emailHelp"
                           class="form-text text-danger"><?php ;
                        echo $error ? $error : ''; ?></small>
                </div>
                <?php
            }
            ?>
            <div class="d-flex justify-content-end">
                <button class="btn btn-primary">Inscription</button>
            </div>
        </form>
    </div>
</div>
